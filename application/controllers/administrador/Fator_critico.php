<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fator_critico extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    if (!$this->ion_auth->in_group('administrador')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a administradores</div>');
			redirect('auth/login');
		}
  	}

	public function index() {
		$this->listar();
	}

	public function listar() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Fator Crítico');
		$crud->set_table('fator_critico');
		$crud->columns('nome');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_export();

		//FORMS
		$crud->order_by('nome','asc');
		$crud->fields('nome');

		//FORM RULES
		$crud->required_fields('nome');

		//EXTRAS
		$extra = array(
				'titulo' => 'Fatores Críticos'
				);

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

    function _crud_output($output = null, $extra = null) {

    	$output->extra = $extra;

        $this->load->view('includes/header');    
        $this->load->view('default', $output);    
        $this->load->view('includes/footer');    
    }
}