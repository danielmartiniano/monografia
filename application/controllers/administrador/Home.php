<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    // Load librarys
	    $this->load->library('ion_auth');

	    // Load models
	    if (!$this->ion_auth->in_group('administrador')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a administradores</div>');
			redirect('auth/login');
		}
  	}

	public function index()	{
		$this->load->view('includes/header');    
        $this->load->view('administrador/home');    
        $this->load->view('includes/footer');  
	}

}