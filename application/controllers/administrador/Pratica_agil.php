<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pratica_agil extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    if (!$this->ion_auth->in_group('administrador')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a administradores</div>');
			redirect('auth/login');
		}
  	}

	public function index() {
		$this->listar();
	}

	public function listar() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Prática Ágil');
		$crud->set_table('pratica_agil');
		$crud->columns('nome');
		$crud->unset_print();
		$crud->unset_export();

		$crud->set_relation_n_n('fator_critico', 'fator_pratica', 'fator_critico', 'pratica_agil_id', 'fator_critico_id', 'nome');

		//ALIAS NAME
		$crud->display_as('descricao','Descrição');
		$crud->display_as('fator_critico','Fatores Críticos');

		//FORMS
		$crud->order_by('nome','asc');
		$crud->fields('nome','descricao','peso','fator_critico');

		//FORM RULES
		$crud->required_fields('nome','peso');

		//EXTRAS
		$extra = array(
				'titulo' => 'Práticas Ágeis'
				);

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

    function _crud_output($output = null, $extra = null) {

    	$output->extra = $extra;

        $this->load->view('includes/header');    
        $this->load->view('default', $output);    
        $this->load->view('includes/footer');    
    }
}