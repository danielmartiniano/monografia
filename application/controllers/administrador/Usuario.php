<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    // Load librarys
	    $this->load->library('ion_auth');
	    $this->load->library('grocery_CRUD');

	    // Load models
	    $this->load->model('ion_auth_model');
	    $this->load->model('intranet/Grocery_crud_model');

	    if (!$this->ion_auth->in_group('admin')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a administradores</div>');
			redirect('auth/login');
		}
  	}

  	public function index() {
		$this->listar();
	}

	public function listar() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Usuário');		 
		$crud->set_table('users');
		$crud->columns('username','first_name','last_name','email');
		$crud->order_by('first_name','asc');
		$crud->unset_add();
		$crud->unset_edit();
		$crud->add_action('','','auth/edit_user','fa fa-pencil');
		$crud->add_action('','','auth/deactivate','fa fa-close');
		$state = $crud->getState();
		
		//ALIAS NAME
		$crud->display_as('username','Usuário')
			 ->display_as('first_name','Nome')
			 ->display_as('last_name','Sobrenome')
			 ->display_as('password','Senha')
			 ->display_as('email','E-mail')
			 ->display_as('created_on','Cadastro')
			 ->display_as('company','Empresa')
			 ->display_as('photo','Foto')
			 ->display_as('phone','Telefone');

		if  ($state == 'read') {
			$crud->unset_fields('ip_address','password','salt','activation_code','forgotten_password_code','forgotten_password_time','remember_code','active','last_login','created_on');
		}

		//EXTRAS
		$extra = array(
				'titulo' => '<span class="go-gray">Intranet ></span> Usuários', 
				'header' => '',
				'alert'  => ''
				);

		if($state == 'list') {
			$extra['header'] = '<a class="btn btn-default" href="'.base_url().'auth/create_user"><i class="fa fa-plus"></i> &nbsp; Adicionar Usuários</a>';
		}

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

     function _crud_output($output = null, $extra = null) {

    	$output->extra = $extra;

        $this->load->view('intranet/includes/header');    
        $this->load->view('intranet/includes/sidebar');    
        $this->load->view('intranet/paginas',$output);    
        $this->load->view('intranet/includes/footer');    
    }

}

/* End of file usuario.php */
/* Location: ./application/controllers/usuario.php */