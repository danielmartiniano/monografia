<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionario extends CI_Controller {

		public function __construct() {
	    parent::__construct();

	    if (!$this->ion_auth->in_group('administrador')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a administradores</div>');
			redirect('auth/login');
		}
  	}

	public function index() {
		$this->listar();
	}

	public function listar() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Pergunta');
		$crud->set_table('pergunta');
		$crud->columns('nome');
		$crud->unset_texteditor('descricao');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_export();

		$crud->set_relation('grupo','pergunta_grupo','nome');

		$crud->set_relation_n_n('fator_critico', 'fator_pergunta', 'fator_critico', 'pergunta_id', 'fator_critico_id', 'nome');

		//ALIAS NAME
		$crud->display_as('descricao','Descrição');
		$crud->display_as('fator_critico','Fatores Críticos');
		$crud->display_as('grupo','Grupo da Pergunta');

		//FORMS
		$crud->order_by('nome','asc');
		$crud->fields('nome','descricao','grupo','fator_critico');

		//FORM RULES
		$crud->required_fields('nome','peso');

		//EXTRAS
		$extra = array(
				'titulo' => 'Questionário',
				'header' => ''
				);

		if ($crud->getState() == 'list') {
			$extra['header'] = '<a href="questionario/grupo_pergunta" class="btn btn-success action-btn">Grupo de Perguntas</a>';
		}

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

    public function grupo_pergunta() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Grupo de Pergunta');
		$crud->set_table('pergunta_grupo');
		$crud->columns('nome');
		$crud->unset_texteditor('descricao');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_export();
		$crud->unset_read();

		//ALIAS NAME
		$crud->display_as('descricao','Descrição');

		//FORMS
		$crud->order_by('nome','asc');
		$crud->fields('nome','descricao');

		//FORM RULES
		$crud->required_fields('nome', 'descricao');

		//EXTRAS
		$extra = array(
				'titulo' => 'Grupos de Pergunta'
				);

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

    function _crud_output($output = null, $extra = null) {

    	$output->extra = $extra;

        $this->load->view('includes/header');    
        $this->load->view('default', $output);    
        $this->load->view('includes/footer');    
    }
}