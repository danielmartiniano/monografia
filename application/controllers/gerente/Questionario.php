<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionario extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    if (!$this->ion_auth->in_group('gerente')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a usuários do sistema</div>');
			redirect('home');
		}

	    //Carrega Model, Helper e Library
	    $this->load->model('questionario_model');
	    $this->load->helper('form');
		$this->load->library('form_validation');
  	}

	public function index() {
		
		//verifico se o gerente ja possui um perfil, caso nao, redireciono ele para a criacao de um
		$perfil = $this->questionario_model->checkPerfil($this->ion_auth->user()->row()->id);

		if (!$perfil) {

			$this->load->view('includes/header');    
	        $this->load->view('gerente/questionario_alerta');    
	        $this->load->view('includes/footer');   

	        return true;
		} 

		$data['perfil'] = $this->questionario_model->getPerfil($this->ion_auth->user()->row()->id);
		
		//se o gerente ja possui um perfil, verifico se ele ja ordenou os fatores
		$checkFator = $this->questionario_model->checkFator($data['perfil'][0]->id);

		if (!$checkFator) {
			
			$this->load->view('includes/header');    
	        $this->load->view('gerente/questionario_alerta', $data);    
	        $this->load->view('includes/footer'); 

	        return true;
		} 

		$data['grupos']    = $this->questionario_model->getGrupos_perguntas();
		$data['perguntas'] = $this->questionario_model->getPerguntas();

		//carrego as respostas do questionario ja respondidas
		$data['respostas'] = $this->questionario_model->getRespostas($data['perfil'][0]->id);

		$this->load->view('includes/header');    
        $this->load->view('gerente/questionario', $data);
        $this->load->view('includes/footer');   
	}

	public function salvar() {
		
		//valido as respostas para evitar erros e ataques
		$this->form_validation->set_rules('pergunta[][]', 'Data', 'trim|xss_clean|is_natural_no_zero|exact_length[1]|in_list[1,2,3,4,5]');

		if ($this->form_validation->run() == FALSE) {
			
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Erro ao salvar questionário!</div>');

		} else {
			$data = $this->input->post(NULL, TRUE);
			
			$this->questionario_model->SetRespostas($data);

			$this->session->set_flashdata('message', '<div class="alert alert-success">Salvo com sucesso! Você pode continuar respondendo ao questionário ou <a href="relatorio/gerar/'.$this->input->post('perfil').'"><strong>gerar o relatório</strong></a></div>');
		}

		redirect('gerente/questionario');

	}
}