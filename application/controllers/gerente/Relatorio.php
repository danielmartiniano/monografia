<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    if (!$this->ion_auth->in_group('gerente')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a usuários do sistema</div>');
			redirect('home');
		}

		$this->load->model('perfil_model');
		$this->load->model('questionario_model');
		$this->load->model('relatorio_model');
  	}

	public function index() {
		$this->listar();
	}

	public function listar() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Relatório');
		$crud->set_table('perfil');
		$crud->columns('nome');
		$crud->where('usuario_id', $this->ion_auth->user()->row()->id);
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_operations();

		//ACTIONS E CALLBACKS
		$crud->add_action('Gerar Relatório', '', 'gerente/relatorio/gerar','fa fa-file-o');
		
		//FORMS
		$crud->order_by('nome','asc');
		$crud->fields('nome');

		//FORM RULES
		$crud->required_fields('nome');

		//EXTRAS
		$extra = array(
				'titulo' => 'Relatórios de Análise',
				'informacao' => 'Após criar seu Perfil, definir a ordem dos Fatores Críticos e responder ao Questionário,
								nesta página você poderá gerar o relatório do sistema com as recomendações de práticas ágeis
								e metodologias ágeis para aplicar em sua equipe. Dentro do relatório você tem a opção de 
								impressão do relatório, Bom trabalho!'
				);

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

    public function gerar($perfil) {

    	$data['perfil']        = $this->perfil_model->getPerfil($perfil);
    	$data['perguntas']     = $this->relatorio_model->getPerguntasRespondidas($perfil);

    	//se o usuario respondeu a pelo menos uma pergunta do questionario, uso as informacoes para gerar o relatorio
    	if (!empty($data['perguntas'])) {
	    	$data['praticas']      = $this->relatorio_model->getPraticas($perfil);
	    	//$data['praticas']      = $this->relatorio_model->getPraticasVencedoras($data['praticas']);
	    	$data['ranking']   	   = $this->relatorio_model->getRankingPraticas($data['praticas']);
	    	$data['praticasAgeis'] = $this->relatorio_model->getPraticasMetodologias($data['ranking']);
	    	$data['metodologias']  = $this->relatorio_model->getMetodologias($data['praticasAgeis']);
    	}

    	$this->load->view('includes/header');    
        $this->load->view('gerente/relatorio', $data);    
        $this->load->view('includes/footer');    

    }

    function _crud_output($output = null, $extra = null) {

    	$output->extra = $extra;

        $this->load->view('includes/header');    
        $this->load->view('default', $output);    
        $this->load->view('includes/footer');    
    }
}