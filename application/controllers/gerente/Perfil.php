<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    if (!$this->ion_auth->in_group('gerente')) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Área restrita a usuários do sistema</div>');
			redirect('home');
		}

		$this->load->model('perfil_model');
  	}

	public function index() {
		$this->listar();
	}

	public function listar() {
        $crud = new grocery_CRUD();

		//CONFIG
		$crud->set_subject('Perfil');
		$crud->set_table('perfil');
		$crud->columns('nome','ativo');
		$crud->where('usuario_id', $this->ion_auth->user()->row()->id);
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_export();
		$crud->unset_read();
		$state = $crud->getState();

		//ACTIONS E CALLBACKS
		$crud->add_action('Ordem de fatores', '', 'gerente/perfil/ordem','fa fa-list-ol');
		$crud->add_action('Tornar Ativo', '', 'gerente/perfil/ativo','fa fa-star-o');
		$crud->callback_column('ativo',array($this,'_callback_ativo'));
		$crud->callback_before_insert(array($this,'set_perfil_ativo'));
		$crud->callback_after_delete(array($this,'delete_perfil'));


		//FORMS
		$crud->order_by('nome','asc');
		$crud->fields('nome','tamanho_equipe','usuario_id');
		$crud->field_type('tamanho_equipe', 'integer');
		$crud->field_type('usuario_id', 'hidden', $this->ion_auth->user()->row()->id);

		//ALIAS NAME
		$crud->display_as('tamanho_equipe','Tamanho da equipe');

		//FORM RULES
		$crud->required_fields('nome','tamanho_equipe');

		//EXTRAS
		$extra = array(
				'titulo' 	 => 'Meus Perfis',
				'informacao' => ''
				);

		if($state == 'list') {
			$extra['informacao'] = 'Nesta página você tem acesso a sua lista de perfis da sua conta! <hr>
									<strong>Ações Disponíveis</strong><br><br>
									<i class="fa fa-pencil"></i> Edição do perfil, mude o nome ou o tamanho da sua equipe<br>
									<i class="fa fa-list-ol"></i> Lista de Fatores, a primeira ação que você deve fazer após criar um perfil. ordene os fatores seguindo as orientações na tela<br>
									<i class="fa fa-star-o"></i> Você pode ter um perfil ativo por vez, ao responder ao questionário, as respostas serão salvas em seu perfil ativo<br>
									<i class="fa fa-trash-o"></i> Botão para deletar um perfil, mas lembre-se, todas as informações do questionário e relatório também serão deletadas';
		}

		if($state == 'add') {
			$extra['informacao'] = 'Informe os dados do seu novo perfil. Ele será automaticamente definido como seu perfil ativo';
		}

		//RENDER
		$output = $crud->render();
		$this->_crud_output($output, $extra);   
    }

    public function ativo($perfil) {
    	
    	//pego usuario ativo para evitar que gerentes mudem perfis de outras contas
    	$usuario = $this->ion_auth->user()->row()->id; 

    	$this->perfil_model->setAtivo($usuario, $perfil);
    	redirect('gerente/perfil');
    }

    function ordem($perfil) {

    	$data['perfil'] = $this->perfil_model->getPerfil($perfil);
    	$data['fatores_criticos'] = $this->perfil_model->getFatoresCriticos($perfil);

    	$this->load->view('includes/header');    
        $this->load->view('gerente/perfil', $data);
        $this->load->view('includes/footer');   
    }

    function salva_ordem() {
    	$perfil = $this->input->post('perfil');
    	$lista  = $this->input->post('lista');
    	
    	//checo se o perfil realmente pertence ao usuario logado
		$check = $this->perfil_model->checkPerfil($this->ion_auth->user()->row()->id, $perfil);
    
    	if ($check == true) {
			$this->perfil_model->salvaOrdem($perfil, $lista);
		}

		return false;
	}

    function _crud_output($output = null, $extra = null) {

    	$output->extra = $extra;

        $this->load->view('includes/header');    
        $this->load->view('default', $output);    
        $this->load->view('includes/footer');    
    }

    public function _callback_ativo($value, $row) {
    	if ($value == 1) {
	  		return "<i class='fa fa-star fa-estrela' aria-hidden='true'></i> <strong>Perfil Ativo</strong>";
    	}
	}

	function set_perfil_ativo($post_array) {
				
		//Verifico se o perfil sendo criado eh o unico do usuario, caso sim, return true, pois no banco o valor default de ativo = 1
		$this->db->where('usuario_id', $post_array['usuario_id']);
		$existe = $this->db->get('perfil')->num_rows();

		if ($existe > 0) {

			//seto o perfil ativo atual como inativo
			$this->db->set('ativo', 0);
			$this->db->where('usuario_id', $post_array['usuario_id']);
			$this->db->update('perfil', $data);
		}

		return true;
	}	 
 
}