<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Homepage extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('ion_auth');
	}

	public function index() {
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('login');
		} else {
			
			if ($this->ion_auth->in_group('administrador')) {
				$this->load->view('includes/header');
				$this->load->view('administrador/home');
				$this->load->view('includes/footer');
			}
			
			if ($this->ion_auth->in_group('gerente')) {
				$this->load->view('includes/header');
				$this->load->view('gerente/home');
				$this->load->view('includes/footer');
			}
		}
	}

	// log the user in
	function login()
	{
		//validate form input
		$this->form_validation->set_rules('identity', '<strong>Usuário</strong>', 'required');
		$this->form_validation->set_rules('password', '<strong>Senha</strong>', 'required');

		if ($this->form_validation->run() == true) {
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
				
				if ($this->ion_auth->in_group('administrador')) {
					redirect('administrador/home');
				}
				if ($this->ion_auth->in_group('gerente')) {
					redirect('gerente/home');
				}

			} else {
				// if the login was un-successful, redirect them back to the login page
				$this->session->set_flashdata('message', '<div class="alert alert-danger text-center">' . $this->ion_auth->errors() . '</div>');
				redirect('homepage'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		} else {
			// the user is not logging in so display the login page
			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . validation_errors() . '</div>');
			redirect('homepage'); // use redirects instead of loading views for compatibility with MY_Controller libraries
		}
	}

	// Logout from admin page
	public function logout() {
  		$this->ion_auth->logout();
  		$this->session->set_flashdata('message', '<div class="alert alert-success text-center">Você saiu da intranet</div>');
  		redirect('homepage');
	}

}