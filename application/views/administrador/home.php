<div class="row home-text">
	
	<h2><strong>Administração do sistema</strong></h2>
    
    <p>
    	O papel do administrador no sistema é alimentar a base de dados com informações e a relação entre os dados.<br>
    	Consulta de estatísticas e gráficos devem ser implementadas em uma versão futura do sistema.
    </p>

    <p>
		<strong>Fatores Críticos</strong><br>
		Cadastro dos Fatores Críticos no sistema.<br> 
		Fatores críticos possuem peso dinâmico e dependem da ordem de fatores definida pelo usuário do sistema em seu perfil.
	</p>

	<p>
		<strong>Práticas Ágeis</strong><br>
		Cadastro das Práticas Ágeis no sistema.<br>
		Baseado nos resultados de uma survey apresentada na monografia, cada Prática Ágil possui um peso fixo definido, este peso utilizado na fórmula geral do cálculo de pontos do sistema.<br>
		Uma Prática Ágil é relacionada a N Fatores Críticos do sistema, esta relação é definida por uma tabela apresentada na monografia.
	</p>

	<p>
		<strong>Metodologias Ágeis</strong><br>
		Cadastro das Metodologias Ágeis no sistema.<br>
		A lista de Metodologias Ágeis é relacionada a N Práticas Ágeis do sistema, ela será exibida no relatório se pelo menos 1 Prática Ágil recomendada ao usuário pertencia a uma Metodologia Ágil.
	</p>

	<p>
		<strong>Questionário</strong><br>
		Cadastro das questões que um usuário deverá responder no sistema.<br>
		Uma questão é relacionada a N Fatores Críticos do sistema, esta relação é definida por uma tabela apresentada na monografia.<br>
		Toda questão que o usuário classifica no questionário é utilizada para gerar o relatório final do seu perfil.
	</p>

</div>