<!DOCTYPE HTML>
<html lang="pt-br">
<head>
    <title>GAPRO</title>
  	<meta charset="utf-8">
    <meta name="description" content="Sistema de Apoio a Decisão - Projeto Final de Curso - UNIRIO">
    <meta name="author" content="Daniel Martiniano e Victor Rocha">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>	
   
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mobilemenu.css">
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-latest.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>
</head>
<body>
	<header class="home_page">
		<div class="container">
        	<div class="row">
                <div class="col-md-12">
                    <a href="/monografia/home"><img src="<?php echo base_url(); ?>assets/images/logo.gif" class="logo" alt="GAPRO"></a>
                    <button class="nav-button">menu</button>
                    <ul class="menu">
                        <?php if ($this->ion_auth->in_group('administrador')) { ?>
                            <li><?php echo anchor('/', 'Início'); ?></li>
                            <li><?php echo anchor('/administrador/fator_critico', 'Fatores Críticos'); ?></li>
                            <li><?php echo anchor('/administrador/pratica_agil', 'práticas Ágeis'); ?></li>
                            <li><?php echo anchor('/administrador/metodologia_agil', 'Metodologias Ágeis'); ?></li>
                            <li><?php echo anchor('/administrador/questionario', 'Questionário'); ?></li>
                        <?php } ?>
                        
                        <?php if ($this->ion_auth->in_group('gerente')) { ?>
                            <li><?php echo anchor('/', 'Início'); ?></li>
                            <li><?php echo anchor('/gerente/perfil', 'Perfil'); ?></li>
                            <li><?php echo anchor('/gerente/questionario', 'Questionário'); ?></li>
                            <li><?php echo anchor('/gerente/relatorio', 'Relatório'); ?></li>
                        <?php } ?>
                            <li><?php echo anchor('/homepage/logout', 'Sair'); ?></li>
                    </ul>
                </div>
            </div>
		</div><!--/container-->
	</header>		
	
    <div class="container-fill">
        <div class="container">