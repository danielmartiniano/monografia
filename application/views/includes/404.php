<!DOCTYPE HTML>
<html lang="pt-br">
<head>
    <title>GAPRO</title>
    <meta charset="utf-8">
    <meta name="description" content="Sistema de Apoio a Decisão - Projeto Final de Curso - UNIRIO">
    <meta name="author" content="Daniel Martiniano e Victor Rocha">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/> 
   
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mobilemenu.css">
    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-latest.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>
</head>
<body>
    <header class="home_page"></header>       
    
    <div class="container-fill">
        <div class="erro404">
            404
            <a class="btn btn-lg btn-success text-center" href="<?php echo base_url(); ?>">Acessar o site</a>
        </div>
    </div>

    <footer>
    </footer>
</body>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>

</html>