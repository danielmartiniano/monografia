<div class="row">
	<h1 class="questionario-h1 pull-left">Questionário</h1>
</div>

<br><br>

<?php if (empty($perfil)) { ?>
	
	<div class="row content">
		
	  	<p class="pull-left">
	  		Olá <?php echo $this->ion_auth->user()->row()->first_name; ?>,<br>
	  		Você precisa ter um <strong>perfil ativo</strong> para responder ao questionário.
	  	</p>

	  	<a href="perfil/index/add" class="btn btn-lg btn-success action-btn pull-right">CRIAR MEU PERFIL</a>

	</div>

<?php } else { ?>

	<div class="row content">
		
	  	<p class="pull-left">
	  		Olá <?php echo $this->ion_auth->user()->row()->first_name; ?>,<br>
	  		Você ainda não definiu a prioridade de fatores do seu <strong>perfil ativo</strong><br>
	  		Clique no botão ao lado, siga as instruções e depois retorne para o questionário.
	  	</p>

	  	<a href="perfil/ordem/<?php echo $perfil[0]->id; ?>" class="btn btn-lg btn-success action-btn pull-right">DEFINIR PRIORIDADES</a>

	</div>

<?php }  ?>