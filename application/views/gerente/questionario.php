<div class="row">
	<h1 class="questionario-h1 pull-left">Questionário</h1>
	<p class="perfil-box pull-right">
		<span class="perfil-title">Meu Perfil Ativo</span> <a href="perfil"><i class="fa fa-pencil-square-o pull-right" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="top" title="Trocar perfil"></i></a><br>
		<?php echo $perfil[0]->nome; ?> - <small><?php echo $perfil[0]->tamanho_equipe; ?> pessoas</small>
	</p>
</div>

<br><br>

<div class="row">
    
    <?php echo $this->session->flashdata('message'); ?>
	
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
    	<li role="presentation" class="active"><a href="#instrucao" aria-controls="instrucao" role="tab" data-toggle="tab"><i class="fa fa-list" aria-hidden="true"></i> Instruções</a></li>
    	<?php foreach ($grupos as $grupo){ ?>
	    	<li role="presentation"><a href="#<?php echo $grupo->nome; ?>" aria-controls="<?php echo $grupo->nome; ?>" role="tab" data-toggle="tab"><?php echo $grupo->nome; ?></a></li>
    	<?php } ?>
    </ul>

	<?php 
	echo form_open('gerente/questionario/salvar');

	$data = array('perfil'  => $perfil[0]->id);
	echo form_hidden($data);

	$data = array('name' => 'submit','value' => 'Salvar Respostas','class' => 'btn-questionario');
	echo form_submit($data);
	?>

	    <div class="tab-content">
	    
		    <div role="tabpanel" class="tab-pane active" id="instrucao">

		    	<u>O questionário divide-se em três categorias, cada categoria contém perguntas agrupadas sobre o mesmo tema:</u>
		    	<br><br>
		    	<ul>
		    		<?php foreach ($grupos as $grupo){ ?>
			    		<li>
			    			<strong><?php echo $grupo->nome; ?></strong>
			    			<?php echo $grupo->descricao; ?>
			    		</li>
		    		<?php }?>
		    	</ul>
				
				<hr>

		    	<u>Classifique as questões em que você considera que são um problema a ser resolvido em sua organização:</u>
		    	<br><br>
		    	<ul>
		    		<li>
		    			<strong>Classificação</strong><br>
		    			Encontrou alguma questão que identifique um problema em sua equipe? classifique a questão marcando a quantidade de estrelas <i class="fa fa-star" style="color: #f70 !important;" aria-hidden="true"></i> que melhor identifique o grau do problema. 
		    			2 estrelas (funciona, mas ainda pode melhorar) até 5 estrelas (crítica, muita necessidade de melhora)
		    		</li>
		    		<br>
		    		<li>
		    			<strong>Estrela preta</strong><br>
		    			Caso você não identique uma questão como um problema a ser resolvido, mantenha a estrela vazia (<i class="fa fa-star" aria-hidden="true"></i>)
		    		</li>
		    	</ul>

				<hr>

	    		<i class="fa fa-thumbs-up" aria-hidden="true"></i> <strong>Dica</strong><br>
		    	- Ao lado de cada questão, utilize o ícone <i class="fa fa-question-circle-o custom-tooltip"></i> para ler mais detalhes sobre a questão apresentada
		    	<br>
		    	- Não esqueça de salvar seu questionário clicando no quadro verde 'Salvar Respostas'.

		    </div>
		    
	    	<?php foreach ($grupos as $grupo){ ?>
			    
			    <div role="tabpanel" class="tab-pane" id="<?php echo $grupo->nome; ?>">
				
		    		<?php foreach ($perguntas as $pergunta){ ?>

		    			<?php if ($pergunta->grupo == $grupo->id) { ?>
		    		
							<div class="form-group row pergunta-box">
								
							    <div class="col-md-10 col-xs-12">
								    <p>
								    	<i class="fa fa-question-circle-o custom-tooltip" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="right" title="<?php echo $pergunta->descricao; ?>"></i>
								    	<?php echo $pergunta->nome; ?>
								    </p>
							    </div>

							    <?php if (empty($respostas)) {
							    	$respostas = array();
							    } ?>

							    <fieldset class="rating col-md-2 col-xs-12">
								    <input type="radio" class="checkbox-hide" id="star5-<?php echo $pergunta->id; ?>" name="pergunta[<?php echo $pergunta->id; ?>][]" value="5" <?php if ( (array_key_exists($pergunta->id, $respostas)) && ($respostas[$pergunta->id] == '5') ) { echo "checked"; } ?> /><label for="star5-<?php echo $pergunta->id; ?>">5</label>
								    <input type="radio" class="checkbox-hide" id="star4-<?php echo $pergunta->id; ?>" name="pergunta[<?php echo $pergunta->id; ?>][]" value="4" <?php if ( (array_key_exists($pergunta->id, $respostas)) && ($respostas[$pergunta->id] == '4') ) { echo "checked"; } ?> /><label for="star4-<?php echo $pergunta->id; ?>">4</label>
								    <input type="radio" class="checkbox-hide" id="star3-<?php echo $pergunta->id; ?>" name="pergunta[<?php echo $pergunta->id; ?>][]" value="3" <?php if ( (array_key_exists($pergunta->id, $respostas)) && ($respostas[$pergunta->id] == '3') ) { echo "checked"; } ?> /><label for="star3-<?php echo $pergunta->id; ?>">3</label>
								    <input type="radio" class="checkbox-hide" id="star2-<?php echo $pergunta->id; ?>" name="pergunta[<?php echo $pergunta->id; ?>][]" value="2" <?php if ( (array_key_exists($pergunta->id, $respostas)) && ($respostas[$pergunta->id] == '2') ) { echo "checked"; } ?> /><label for="star2-<?php echo $pergunta->id; ?>">2</label>
								    <input type="radio" class="checkbox-hide" class="star1" id="star1-<?php echo $pergunta->id; ?>" name="pergunta[<?php echo $pergunta->id; ?>][]" value="1" /><label class="star1" for="star1-<?php echo $pergunta->id; ?>">1</label>
								</fieldset>
								<small><?php echo form_error('questionario[pergunta[<?php echo $pergunta->id; ?>]]'); ?></small>

							</div>

		    			<?php } ?>

		    		<?php } ?>

			    </div>
	    	<?php } ?>

	    </div>
	
	<?php echo form_close(); ?>

</div>

<script>
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>