<div class="row home-text">
	
	<h2><strong>GAPRO (Gerência Ágil de Projetos)</strong></h2>
	
	<p>Este sistema é recomendado a todos os profissionais que gerenciam equipes em qualquer área de atuação que envolva projetos, construção civil, arquitetura, time de vendas, empresas de tecnologia, etc...</p>

	<p>O profissional de projetos e sua equipe planejam e coordenam o desenvolvimento de um projeto utilizando uma ampla variedade de métodos, tecnologias e processos, mas esta mesma grande quantidade de opções disponíveis, muitas vezes são utilizadas de forma ineficaz, gerando mais trabalho e resultados considerados não satisfatórios. Cabe ao profissional, conseguir identificar que ferramentas são as mais adequadas para cada tipo de projeto.</p>

	<p>o <strong>GAPRO</strong> auxilia profissionais recomendando práticas ágeis e metodologias ágeis, com o objetivo de orientar o trabalho e otimizar seus resultados em um projeto. Com informações apresentadas pelo próprio profissional sobre o perfil de sua equipe, o sistema consegue recomendar de maneira eficaz opções e práticas que ajudem a diminuir ou exterminar, as dificuldades encontradas da organização.</p>

	<p>
		<strong>Perfis</strong><br>
		Para utilizar o sistema, o primeiro passo é a criação de um perfil. Durante a criação de um perfil, você escolherá um nome e informará o tamanho da sua equipe de desenvolvedores em um projeto ou organização.
	</p>

	<p>Após a criação do perfil você definirá quais são os Fatores (ícone <i class="fa fa-list-ol"></i> na página PERFIL) que você considera como mais importantes na sua equipe, mas que ainda há dificuldades em gerenciar na equipe.</p>

	<p>Por exemplo, se você considera o fator 'Pessoal Qualificado e suficiente' como uma questão a ser resolvida por não ter uma equipe técnica experiente, você deixará este fator como um dos primeiros da lista. Ao mesmo tempo você pode considerar 'Comunicação eficaz e feedback' como um ponto bem resolvido na equipe e manter ele em uma posição mais abaixo da lista.</p>

	<p>
		<strong>Questionário</strong><br>
		Você somente poderá acessar o questionário após a criação de seu perfil e a ordenação da lista de fatores.
		Leia com atenção as instruções da primeira aba que estará disponível no questionário, ela irá apresentar a você as seções disponíveis e como cada questão deve ser classificada.
	</p>

	<p>
		<strong>Relatórios</strong><br>
		Após responder a pelo menos 1 questão do relatório, você poderá gerar o relatório do seu perfil! O relatório apresenta uma série de recomendações sobre que práticas ágeis e metodologias ágeis você pode aplicar em sua equipe para mitigar as dificuldades encontradas pelo time. O relatório é baseado em algumas informações como: a ordem dos fatores em seu perfil, a resposta para cada questão no questionário e dados internos baseados e, estatísticas e consulta a profissionais de projetos.
	</p>

	<p>
		<i class="fa fa-thumbs-up" aria-hidden="true"></i> <strong>Dicas</strong><br>
		Você pode editar seu perfil e suas respostas no questionário quantas vezes achar necessário, mas lembre-se, o relatório é gerado em tempo real baseado nas suas escolhas nestes quesitos. Caso você gerencie mais de uma equipe ou presencia mais de um cenário na sua organização, você pode criar um novo perfil baseado nestas novas informações. Você pode criar quantos perfis quiser no sistema!
	</p>

</div>