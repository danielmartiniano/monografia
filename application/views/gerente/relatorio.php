<link rel="stylesheet" media="print" href="<?php echo base_url(); ?>assets/css/print.css">
<?php //$this->output->enable_profiler(TRUE); ?>

<div class="row">
	<h1 class="questionario-h1 pull-left">Relatório Geral</h1>
	<p class="perfil-box pull-right">
		<span class="perfil-title">PERFIL</span><br>
		<?php echo $perfil[0]->nome; ?> - <small><?php echo $perfil[0]->tamanho_equipe; ?> pessoas</small>
	</p>
</div>
    
<!-- Tab panes -->
<?php if (empty($perguntas)) { ?>

	<div class="alert alert-danger text-center row">
		Você ainda não respondeu ao questionário com o perfil ativo <strong>'<?php echo $perfil[0]->nome; ?>'</strong><br>
		Não é possível gerar um relatório neste momento<br><br>
		<a class="btn btn-success" href="<?php echo base_url(); ?>gerente/questionario">Quero responder o questionário</a>
	</div>
	
<?php } else { ?>

<div class="row content content-objective">
	<p class="lead"><strong>Objetivo do Relatório</strong></p>
	Auxiliar o profissional responsável pela gerência de projetos, na escolha de práticas ágeis para adoção em sua equipe. O relatório será baseado nas questões classificadas pelo profissional durante o questionário e na ordem de relevância dos fatores críticos, definida em seu perfil. O relatório apresentará as práticas recomendadas afim de amenizar e/ou resolver os problemas identificados.
</div>

<ul class="nav nav-tabs relatorio-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#aba1" aria-controls="aba1" role="tab" data-toggle="tab">Práticas Ágeis Por Questão</a></li>
	<li role="presentation"><a href="#aba2" aria-controls="aba2" role="tab" data-toggle="tab">Ranking de Práticas Ágeis</a></li>
	<li role="presentation"><a href="#aba3" aria-controls="aba3" role="tab" data-toggle="tab">Metodologias Ágeis</a></li>
</ul>

<div class="row content content-relatorio">

	<a class="btn btn-primary btn-print" onClick="window.print()"><i class='fa fa-print' aria-hidden='true'></i> Imprimir Relatório</a>

    <div class="tab-content relatorio-tab-content">
	    
	    <div role="tabpanel" class="tab-pane active" id="aba1">

	    	<div class="print-div"><h1>Práticas Ágeis Por Questão Respondida</h1></div>

		    <div class="objective">
		    	Para cada pergunta do questionário que você classificou com pelo menos 2 estrelas, o sistema recomenda que práticas ágeis você pode utilizar para melhorar a questão em sua equipe. Cada problema pode ser resolvido através da adoção de uma ou mais práticas recomendadas. Clique em cada pergunta para verificar as recomendações.
		    </div>

			<hr>

			<button id="collapse-btn" class="btn btn-primary pull-right">Abrir quadros</button>

		    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

				<?php 
				$counter = 0;
				foreach ($perguntas as $pergunta) { 
				$counter++;
				?>
				
					<div class="panel panel-default relatorio-panel">
					    <div class="panel-heading" role="tab"  data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $counter; ?>" aria-expanded="true" aria-controls="collapse<?php echo $counter; ?>">
					        <h4 class="panel-title">
						        <a role="button">
						          <?php echo $pergunta->nome; ?>
						        </a>
					        </h4>
					    </div>
				        <div id="collapse<?php echo $counter; ?>" class="panel-collapse collapse" role="tabpanel">
				        	<div class="panel-body">

				            	<p class="pergunta-descricao">
				            		<small>Exemplo de problema</small><br>
				            		<?php echo $pergunta->descricao; ?>
				            	</p>
				            
					            <table class="table table-bordered table-pratica">
					                <thead>
						                <th>Prática Ágil Recomendada</th>
						                <th>Descrição</th>
					                </thead>
					                <tbody>
					                	<?php foreach ($praticas as $pratica) { ?>

					                		<?php if ($pratica->pergunta_id == $pergunta->id) { ?>

						                		<?php 
												//regra do scrum de scrum, a pratica tem ID = 12
												if ( ($pratica->pratica_agil_id != 12) || ( ($pratica->pratica_agil_id == 12) && ($perfil[0]->tamanho_equipe > 10) ) ) { ?>

								                	<tr>
								                		<td width="23%"><strong><?php echo $pratica->pratica_agil; ?></strong> (<?php echo $pratica->pontos; ?>)</td>
								                		<td class="text-justify"><?php echo $pratica->descricao; ?></td>
								                	</tr>
					                			
						                		<?php } ?>
	
					                		<?php } ?>
					                		
					                	<?php } ?>
					                </tbody>
					            </table>

				          	</div>
			            </div>
			        </div>
				
				<?php } ?>

			</div>

    	</div> <!-- tabpanel -->

    	<div role="tabpanel" class="tab-pane" id="aba2">

    		<div class="print-div"><h1>Ranking de Práticas Ágeis</h1></div>

	  		<div class="objective">
		    	A lista abaixo é um complemento da aba 'Práticas Ágeis Por Questão'. A lista apresenta todas as práticas ágeis recomendadas
		    	e a quantidade de vezes que uma prática foi recomendada após analisar todas as suas questões respondidas no questionário.
		    	Você pode utilizar esta lista para criar um ambiente de projeto com um menor número de práticas que podem resolver o maior número de problemas.
		    </div>

            <table class="table table-bordered table-pratica">
                <thead>
	                <th>Prática Ágil Recomendada</th>
	                <th>Descrição</th>
	                <th width='1'>Pontos</th>
                </thead>
                <tbody>
                	<?php foreach ($ranking as $pratica) { ?>

						<?php 
						//regra do scrum de scrum, a pratica tem ID = 12
						if ( ($pratica['pratica_agil_id'] != 12) || ( ($pratica['pratica_agil_id'] == 12) && ($perfil[0]->tamanho_equipe > 10) ) ) { ?>
								
		                	<tr>
		                		<td width="23%"><?php echo $pratica['pratica_agil']; ?></td>
		                		<td class='text-justify'><?php echo $pratica['descricao']; ?></td>
		                		<td class='text-center'><?php echo $pratica['recomendacoes']; ?></td>
		                	</tr>

						<?php } ?>
                		
                	<?php } ?>
                </tbody>
            </table>
				
	  	</div> <!-- tabpanel -->

	    <div role="tabpanel" class="tab-pane" id="aba3">

	    	<div class="print-div"><h1>Metodologias Ágeis</h1></div>

	  		<div class="objective">
		    	Uma prática ágil faz parte de uma metodologia Ágil, que é um conjunto de práticas e regras que juntas
		    	podem ser aplicadas em um projeto. O quadro abaixo apresenta dentro de cada metodologia ágil cadastrada no sistema,
		    	uma lista com as práticas ágeis que o sistema recomendou para seu perfil. Você pode utilizar estas informações para
		    	verificar a metodologia ágil que possui mais práticas recomendadas e verificar que outras práticas dentro desta mesma 
		    	metodologia você também poderia adotar em seus projetos. Algumas vezes seguir uma única metodologia	pode ser o melhor caminho em um projeto.
		    </div>

            <table class="table table-bordered table-pratica">
                <thead>
	                <th>Metodologia Ágil</th>
	                <th>Descrição</th>
                </thead>
                <tbody>
                	<?php foreach ($metodologias as $metodologia) { ?>

	                	<tr>
	                		<td width="15%"><?php echo $metodologia['nome']; ?></td>
	                		<td class='text-justify'>
	                			<?php echo $metodologia['descricao']; ?>
	                			<br>
	                			<strong>Práticas recomendadas que fazem parte desta metodologia</strong>
	                			<ul class="metodologia-praticas">
	                				<?php foreach ($praticasAgeis as $praticaAgil) { ?>
	                					<?php if ($metodologia['id'] == $praticaAgil['id']) { ?>

	                						<?php 
											//regra do scrum de scrum, a pratica tem ID = 12
											if ( ($praticaAgil['pratica_agil_id'] != 12) || ( ($praticaAgil['pratica_agil_id'] == 12) && ($perfil[0]->tamanho_equipe > 10) ) ) { ?>

			                					<li>
			                						<strong><?php echo $praticaAgil['pratica_agil']; ?></strong><br>
			                						<?php echo strip_tags($praticaAgil['pratica_agil_descricao']); ?>
			                					</li>
			                					<br>
		                					
		                					<?php } ?>

	                					<?php } ?>
	                				<?php } ?>
	                			<ul>
	                		</td>
	                	</tr>
                		
                	<?php } ?>
                </tbody>
            </table>
	    	
	    </div> <!-- tabpanel -->
    
  	</div>
	
<?php }  ?>
	
</div>

<script>
	$(function () {

    var active = true;

    $('#collapse-btn').click(function () {
        if (active) {
            active = false;
            $('.panel-collapse').collapse('show');
            $('.panel-title').attr('data-toggle', '');
            $(this).text('Fechar quadros');
        } else {
            active = true;
            $('.panel-collapse').collapse('hide');
            $('.panel-title').attr('data-toggle', 'collapse');
            $(this).text('Abrir quadros');
        }
    });
    
    $('#accordion').on('show.bs.collapse', function () {
        if (active) $('#accordion').collapse('hide');
    });

});
</script>