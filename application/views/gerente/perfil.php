<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<div class="row">
    <h1 class="questionario-h1 pull-left"><?php echo $perfil[0]->nome; ?></h1>
    <p class="perfil-box pull-right">
        <span class="perfil-title">Meu Perfil Ativo</span> <a href="<?php echo base_url(); ?>gerente/perfil"><i class="fa fa-pencil-square-o pull-right" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="top" title="Trocar perfil"></i></a><br>
        <?php echo $perfil[0]->nome; ?> - <small><?php echo $perfil[0]->tamanho_equipe; ?> pessoas</small>
    </p>
</div>

<br><br>

<div class="row">

	<div class="alert alert-info alert-text">
		<p>
            <strong>Definindo um perfil</strong><br>
            Nesta página, você definirá quais são os fatores que você considera como mais importantes na sua equipe, mas que ainda tem dificuldades em gerenciar na equipe.
            Na lista abaixo, utilize o ícone <i class="fa fa-arrows-v" aria-hidden="true"></i> para arrastar um fator na ordem desejada.
        </p>
        <p>
            <i class="fa fa-star" aria-hidden="true"></i>
            <strong>Exemplo</strong><br> 
            Se você considera o fator 'Pessoal Qualificado e suficiente' como uma questão a ser resolvida pois não tem uma equipe técnica experiente, você deixará este fator como um dos primeiros no topo da lista. Ao mesmo tempo, você pode considerar 'Comunicação eficaz e feedback' como um ponto bem resolvido na equipe e manter ele em uma posição mais abaixo da lista.</p>
        <p>
            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
            <strong>Dica</strong><br>
            Esta lista é salva automaticamente quando você altera a ordem de um item. Quando achar que a lista de fatores está apropriada, utilize o menu principal para começar a responder ao questionário!
        </p>
	</div>

</div>

    <hr>
<br>

<div class="row">

    <div class="result"></div>


	<ul id="sortable">
		<?php foreach ($fatores_criticos as $fator) { ?>
		    <li id="<?php echo $fator->id; ?>">
		        <span></span>
		        <h2><?php echo $fator->nome; ?></h2>
		    </li>
		<?php } ?>
	</ul>

</div>

<script>
$(function() {
    $('#sortable').sortable({
        axis: 'y',
        opacity: 0.7,
        handle: 'span',
        update: function(event, ui) {
            var list_sortable = $(this).sortable('toArray').toString();
            //$('.result').text(list_sortable);

            $.ajax({
                url: '<?php echo base_url(); ?>gerente/perfil/salva_ordem',
                type: 'POST',
                data: {perfil: <?php echo $perfil[0]->id; ?>, lista:list_sortable},
                dataType:"json",
                success: function(data) {
                }
            });
        }
    });
});
</script>