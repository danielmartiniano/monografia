<?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<div class="row">

	<h2 class="page-title"><?php echo $extra['titulo']; ?></h2>
	
	<?php if (isset($extra['header'])) { echo $extra['header']; } ?>

	<div class="clearfix"></div> 

	<hr>
	
		<?php if (isset($extra['informacao'])) { ?>
			<div class="alert alert-info">
				<?php echo $extra['informacao']; ?>
			</div>
		<?php } ?>

	<?php if (isset($message)) { echo $message; } ?>

</div>

<div class="row">
	<?php echo $output; ?>
</div>