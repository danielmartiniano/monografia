<!DOCTYPE HTML>
<html lang="en-us">
<head>
    <title>GAPRO</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/> 
   
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
</head>
<body>
    <header class="home_page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="/monografia/home"><img src="<?php echo base_url(); ?>assets/images/logo.gif" class="logo-login" alt="GAPRO"></a>
                </div>
            </div>
        </div><!--/container-->
    </header>       
    <div class="container-fill">
        <div class="container">

            <div class="col-md-4 login-div">
                <p class="text-center">Faça login no sistema</p>

                <?php echo $this->session->flashdata('message'); ?>

                <?php echo form_open('homepage/login'); ?>
                    <div class="form-group">
                        <?php $data = array('name' => 'identity', 'class' => 'form-control', 'placeholder' => 'Login'); ?>
                        <?php echo form_input($data); ?>
                    </div>
                    <div class="form-group">
                        <?php $data = array('name' => 'password', 'class' => 'form-control', 'placeholder' => 'Senha'); ?>
                        <?php echo form_password($data); ?>
                    </div>
                        <?php $data = array('name' => 'submit', 'value' => 'Acessar', 'class' => 'btn btn-success pull-right'); ?>
                        <?php echo form_submit($data); ?>
                <?php echo form_close(); ?>
            </div>
            
        </div>
    </div>
</body>
</html>