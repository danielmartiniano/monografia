<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questionario_model extends CI_Model {

	/**
	 * Verifica se o usuario tem um perfil criado
	 * @param int $usuario 
	 * @return bool
	 */
	public function checkPerfil($usuario) {
		$this->db->where('usuario_id', $usuario);
		$query = $this->db->get('perfil');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Verifica se o usuario tem um perfil criado
	 * @param int $usuario 
	 * @return bool
	 */
	public function checkFator($perfil) {
		$this->db->where('perfil_id', $perfil);
		$query = $this->db->get('fator_perfil');

		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Carrega o perfil do gerente
 	 * @param int $usuario 
	 * @return array
	 */
	public function getPerfil($usuario) {
		$this->db->where('usuario_id', $usuario);
		$this->db->where('ativo', 1);
		$query = $this->db->get('perfil');

		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
		
	}

	/**
	 * Carrega os grupos de perguntas
	 */
	public function getGrupos_perguntas() {
		$query = $this->db->get('pergunta_grupo');
		return $query->result();
	}

	/**
	 * Carrega as perguntas de um grupo
	 */
	public function getPerguntas() {
		$query = $this->db->get('pergunta');
		return $query->result();
	}

	/**
	 * Salva respostas do gerente
	 * @param array $data 
	 * @return boolean
	 */
	public function SetRespostas($data) {

		$perfil   = $data['perfil'];
		$pergunta = $data['pergunta'];

		//ordena o array, somente pela organizacao
		ksort($pergunta);

		//salva no banco as respostas
		foreach ($pergunta as $key => $value) {

			//verifica se ja existe resposta para a pergunta no perfil ativo
			$this->db->where('pergunta_id', $key);
			$this->db->where('perfil_id', $perfil);
			$existe = $this->db->get('resposta')->num_rows();

			//se ja existe resposta, atualiza a resposta
			if ($existe > 0) {

				//nao usamos a 'estrela preta' para gerar o relatorio, entao deletamos os registros dela no banco quando o gerente atualiza uma resposta para 'estrela preta'
				if ($value[0] == 1) {
					
					$this->db->where('pergunta_id', $key);
					$this->db->where('perfil_id', $perfil);
					$this->db->delete('resposta');

				} else {
				
					$dataQuery = array('resposta' => $value[0]);
					$this->db->where('pergunta_id', $key);
					$this->db->where('perfil_id', $perfil);
					$this->db->update('resposta', $dataQuery);
				}

			//senao, salva nova resposta
			} else {
				
				if ($value[0] > 1) {

					$dataQuery = array('perfil_id' => $perfil,'pergunta_id' => $key, 'resposta' => $value[0]);
					$this->db->insert('resposta', $dataQuery);
				}
			}

		}

		return true;
	}

	/**
	 * Carrega respostas do perfil de um gerente 
	 * @param int $perfil 
	 * @return array
	 */
	public function getRespostas($perfil) {
		$this->db->where('perfil_id', $perfil);
		$respostas = $this->db->get('resposta')->result();

		//a consulta acima ja fez o select identificando o perfil, agora posso criar novo array somente com a ID da pergunta e resposta
		foreach ($respostas as $valueResposta) {
			$arrayRespostas[$valueResposta->pergunta_id] = $valueResposta->resposta;
		}
		
		if (!empty($arrayRespostas)) {
			return $arrayRespostas;
		} else {
			return false;
		}
	}

}