<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_model extends CI_Model {

	/**
	 * Seleciona informacoes de um perfil
	 * @param int $perfil 
	 * @return array
	 */
	public function getPerfil($perfil) {
		$this->db->where('id', $perfil);
		$query = $this->db->get('perfil');
		return $query->result();
	}

	/**
	 * Seleciona todos os fatores_Criticos
	 */
	public function getFatoresCriticos($perfil) {
		$this->db->select('fator_critico.id as id, fator_critico.nome as nome');
		$this->db->join('fator_perfil', 'fator_perfil.fator_critico_id = fator_critico.id');
		$this->db->where('perfil_id', $perfil);
		$this->db->order_by('prioridade', 'asc');
		$query = $this->db->get('fator_critico');

		//se o gerente ja possui um perfil com fatores criticos ordenados, carrego o perfil na ordem correta
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			//se o gerente ainda nao ordenou os fatores nenhuma vez, somente lista fatores
			$query = $this->db->get('fator_critico');
			return $query->result();
		}
	}

	/**
	 * Define qual o perfil ativo do gerente
	 * @param int $usuario 
	 * @param int $perfil 
	 * @return bool
	 */
	public function setAtivo($usuario, $perfil) {

		//Verifico se eh o unico perfil do usuario, caso sim, so faco o update para ativo
		$this->db->where('usuario_id', $usuario);
		$unico = $this->db->get('perfil')->num_rows();

		if ($unico > 1) {

			//seto o perfil ativo atual como inativo
			$this->db->set('ativo', 0);
			$this->db->where('usuario_id', $usuario);
			$this->db->where('ativo', 1);
			$this->db->update('perfil', $data);
		}

		//seto o perfil selecionado como ativo
		$this->db->set('ativo', 1);
		$this->db->where('usuario_id', $usuario);
		$this->db->where('id', $perfil);
		$this->db->update('perfil', $data);

		return true;
	}

	/**
	 * Checa se um perfil pertence ao usuario logado
	 * @param int $usuario 
	 * @param int $perfil 
	 * @return bool
	 */
	public function checkPerfil($usuario, $perfil) {
		$this->db->where('id', $perfil);
		$this->db->where('usuario_id', $usuario);
		$query = $this->db->get('perfil');

		if ($query->num_rows() > 0) {
			return true;
		}

		return false;
	}

	/**
	 * Salva a ordem de fatores criticos no perfil do gerente
	 * @param int $perfil 
	 * @param string $lista 
	 * @return bool
	 */
	public function salvaOrdem($perfil, $lista) {
		
		//converte ordem de string em array, comecando com index = 1
		$explode = explode(',', $lista);
		$lista   = array_combine(range(1, count($explode)), $explode);

		//checo se ja existe uma ordem para este perfil
		$this->db->where('perfil_id', $perfil);
		$query  = $this->db->get('fator_perfil');
		$existe = $query->num_rows();
		
		if ($existe > 0) {
	
			//se ja existe ordem para este perfil, atualizo valores
			foreach ($lista as $key => $value) {
				$this->db->set('prioridade', $key);
				$this->db->where('fator_critico_id', $value);
				$this->db->where('perfil_id', $perfil);
				$this->db->update('fator_perfil');
			}

		//senao, crio ordem pela primeira vez
		} else {
				
			foreach ($lista as $key => $value) {
				$data = array('perfil_id' => $perfil, 'prioridade' => $key, 'fator_critico_id' => $value);
				$this->db->insert('fator_perfil', $data);
			}
		}

		return $this->db->affected_rows() > 0;
	}

}