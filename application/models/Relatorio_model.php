<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorio_model extends CI_Model {

	/**
	 * pega o numero de fatores, valor base para a formula geral
	 */
	public function getNumeroFatoresCriticos() {
		$query = $this->db->get('fator_critico');
		 return $query->num_rows();
	}

	/**
	 * Pega as perguntas que o gerente respondeu
	 * @param int $perfil
	 * @return array
	 */
	public function getPerguntasRespondidas($perfil) {
		$this->db->select('pergunta.id as id, pergunta.nome as nome, pergunta.descricao as descricao');
		$this->db->join('resposta', 'pergunta.id = resposta.pergunta_id');
		$this->db->where('resposta.perfil_id', $perfil);
		$query = $this->db->get('pergunta');

		if ($query->num_rows() > 0) {
			return $query->result();
		}

		return false;
	}

	/**
	 * Calcula peso de um fator critico dentro do perfil ativo do gerente
	 * Peso do fator critico = (Base / nFatores) * ((nFatores – posicaoRanking) + 1)
	 * @param int $posicaoRanking
	 * @return int
	 */
	public function getPesoFatorCritico($posicaoRanking) {
		$base = 100;
		$nFatores = $this->getNumeroFatoresCriticos();

		return ( ($base / $nFatores) * (($nFatores - $posicaoRanking) + 1) );
	}

	/**
	 * Calcula peso da resposta de uma pergunta dentro do perfil ativo do gerente
	 * @param int $resposta
	 * @return int
	 */
	public function getPesoResposta($resposta) {

		switch ($resposta) {
			case '2':
				$pesoResposta = 1;
				break;
			case '3':
				$pesoResposta = 2;
				break;
			case '4': 
			case '5':
				$pesoResposta = 3;
				break;
			default:
				return false;
				break;
		}

		return $pesoResposta;
	}

	/**
	 * Pega as praticas ageis e calcula sua pontuacao
	 * @param int $perfil
	 * @return array
	 */
	public function getRelacaoPraticas($perfil) {
		$this->db->select('pergunta.id as pergunta_id, pergunta.nome as pergunta, resposta.resposta as resposta, fator_critico.nome as fator_critico, fator_perfil.prioridade as prioridade, pratica_agil.id as pratica_agil_id, pratica_agil.nome as pratica_agil, pratica_agil.descricao as descricao, pratica_agil.peso as pratica_agil_peso');
		$this->db->join('fator_pergunta', 'pergunta.id = fator_pergunta.pergunta_id ');
		$this->db->join('fator_critico', 'fator_critico.id = fator_pergunta.fator_critico_id');
		$this->db->join('fator_pratica', 'fator_critico.id = fator_pratica.fator_critico_id');
		$this->db->join('pratica_agil', 'pratica_agil.id = fator_pratica.pratica_agil_id');
		$this->db->join('fator_perfil', 'fator_critico.id = fator_perfil.fator_critico_id');
		$this->db->join('perfil', 'perfil.id = fator_perfil.perfil_id');
		$this->db->join('resposta', 'resposta.pergunta_id = pergunta.id');

		$this->db->where('resposta.perfil_id', $perfil);
		$this->db->where('fator_perfil.perfil_id', $perfil);
		$query = $this->db->get('pergunta');

		if ($query->num_rows() > 0) {
			return $query->result();
		}

		return false;
	}

	/**
	 * Calcula a pontuacao de uma pergunta
	 * Pontos = Pfc * (Pp + Pr), Pfc – Peso do Fator Crítico; Pp – Peso da Prática; Pr – Peso da Resposta
	 * @param int $pontos_fator
	 * @param int $pontos_resposta
	 * @param int $pontos_pratica
	 * @return int
	 */
	public function getPontuacaoGeral($pontos_fator, $pontos_resposta, $pontos_pratica) {
		$pontos = $pontos_fator * ($pontos_pratica + $pontos_resposta);
		
		return round($pontos);
	}

	/**
	 * Aba 1 - Pega as praticas relacionadas a pergunta utilizando a formula geral
	 * @param int $perfil
	 * @return array
	 */
	public function getPraticas($perfil) {
		
		$dados = array();

		$dados = $this->getRelacaoPraticas($perfil); //array com relacao das praticas

		// Para cada pergunta, calculo os pontos das praticas ageis
		foreach ($dados as $praticas) {
			$pontos_fator	 = $this->getPesoFatorCritico($praticas->prioridade);
			$pontos_resposta = $this->getPesoResposta($praticas->resposta);
			$pontos_pratica  = $praticas->pratica_agil_peso;

			//coloco a pontuacao da pergunta no array
			$praticas->pontos = round($this->getPontuacaoGeral($pontos_fator, $pontos_resposta, $pontos_pratica));
		}

 		//deixo no array somente as praticas 'acima da media'
 		//$dados = $this->getPraticasVencedoras($data['praticas']);

		//ordeno pontos de modo crescente, para remover praticas duplicadas com menor pontuacao
		usort($dados, array($this,'orderByPontosCrescente'));

 		//remove praticas duplicadas em uma pergunta
 		foreach ($dados as $key => $value) {
 			foreach ($dados as $pratica) {
 				if ( ($pratica->pergunta_id == $value->pergunta_id) && ($pratica->pratica_agil == $value->pratica_agil) && ($pratica->fator_critico != $value->fator_critico) ) {
 					unset($dados[$key]);
 				}
 			}
 		}

		//ordeno array de modo decredcente para mostrar na lista as praticas com mais pontuacoes no topo
		usort($dados, array($this,'orderByPontosDecrescente'));

		return $dados;
	}

	/**
	 * Pega as praticas 'vencedoras' utilizando a media de pontos da pratica
	 * @param array $praticas
	 * @return array
	 */
	public function getPraticasVencedoras($praticas) {
		$pontuacao = 0;
		$tamanho   = sizeof($praticas);

		//pontuacao total das praticas
		foreach ($praticas as $pratica) {
			$pontuacao = $pontuacao + $pratica->pontos;
		}

		//media de pontos das praticas
		$media = $pontuacao / $tamanho;

		//retiro do array as praticas 'perdedoras'
		foreach ($praticas as $key => $value) {
			if ($value->pontos < $media) {
				unset($praticas[$key]);
			}
		}

		return $praticas;
	}

	/**
	 * Aba 2 - Pega as praticas e conta quantas vezes cada pratica eh recomendada
	 * @param array $praticas
	 * @return array
	 */
	public function getRankingPraticas($praticas) {
		
		$ranking = array();
		 
		foreach ($praticas as $pratica) {
			
			//crio index (posicao) para cada pratica agil
			if (!array_key_exists($pratica->pratica_agil, $ranking)) {

				$ranking[$pratica->pratica_agil] = array('pratica_agil_id' => $pratica->pratica_agil_id, 
														 'pratica_agil' => $pratica->pratica_agil, 
														 'descricao' => $pratica->descricao, 
														 'recomendacoes' => 1
														 );
			
			//se ja existe index, incremento seu contador
			} else {
				$ranking[$pratica->pratica_agil]['recomendacoes']++;
			}
		}
		
		//ordeno pelo numero de recomendacoes
 		usort($ranking, array($this,'orderByRecomendacoesDecrescente'));

 		return $ranking;
	}

	/**
	 * Aba 3 - Pega as praticas ageis recomendadas para exibir na aba de metodologias
	 * @param array $ranking
	 * @return array
	 */
	public function getPraticasMetodologias($ranking) {
		$this->db->select('metodologia_agil.id as id, metodologia_agil.nome as nome, metodologia_agil.descricao as descricao, pratica_metodologia.pratica_agil_id as pratica_agil_id');
		$this->db->join('pratica_metodologia', 'pratica_metodologia.metodologia_agil_id = metodologia_agil.id');
		$query = $this->db->get('metodologia_agil')->result();

		foreach ($ranking as $pratica_agil) {
			foreach ($query as $metodologia_agil) {
				if ($pratica_agil['pratica_agil_id'] == $metodologia_agil->pratica_agil_id) {
					$metodologias[] = array('id' => $metodologia_agil->id, 'nome' => $metodologia_agil->nome, 'descricao' => $metodologia_agil->descricao, 'pratica_agil_id' => $pratica_agil['pratica_agil_id'], 'pratica_agil' => $pratica_agil['pratica_agil'], 'pratica_agil_descricao' => $pratica_agil['descricao']);
				}
			}
		}

		return $metodologias;
	}

	/**
	 * Aba 3 - Pega as metodologias que contem praticas recomendadas
	 * @param array $praticas_ageis
	 * @return array
	 */
	public function getMetodologias($praticas_ageis) {
	    $i = 0; 
	    $metodologias = array(); 
	    $key_array    = array(); 
	    
	    foreach($praticas_ageis as $val) { 
	        if (!in_array($val['id'], $key_array)) { 
	            $key_array[$i] = $val['id']; 
	            $metodologias[$i] = array('id' => $val['id'], 'nome' => $val['nome'], 'descricao' => $val['descricao']); 
	        } 
	        $i++; 
	    } 

	    return $metodologias; 
	}

	/**
	 * Ordena um array pelo seu numero de recomendacoes, ordenacao decrescente
	 * @param array $a
	 * @param array $b
	 * @return array
	 */
	function orderByRecomendacoesDecrescente($a, $b) { 
		return $b['recomendacoes'] - $a['recomendacoes']; 
	}

	/**
	 * Ordena um array pela sua pontuacao, ordenacao crescente
	 * @param array $a
	 * @param array $b
	 * @return array
	 */
	public function orderByPontosCrescente ($a, $b) { 
		return $a->pontos - $b->pontos; 
	}

	/**
	 * Ordena um array pela sua pontuacao, ordenacao decrescente
	 * @param array $a
	 * @param array $b
	 * @return array
	 */
	public function orderByPontosDecrescente ($a, $b) { 
		return $b->pontos - $a->pontos; 
	}

}