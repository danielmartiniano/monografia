-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 06-Jul-2016 às 16:07
-- Versão do servidor: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `monografia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `fator_critico`
--

CREATE TABLE IF NOT EXISTS `fator_critico` (
`id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `fator_critico`
--

INSERT INTO `fator_critico` (`id`, `nome`) VALUES
(1, 'Requisitos e especificações claras'),
(2, 'Objetivos e metas claros'),
(3, 'Prazos realistas'),
(4, 'Apoio da alta administração'),
(5, 'Usuário / envolvimento do cliente'),
(6, 'Comunicação eficaz e feedback'),
(7, 'Orçamento realista'),
(8, 'Pessoal qualificado e suficiente'),
(9, 'A familiaridade com a tecnologia de desenvolvimento'),
(10, 'A familiaridade com a metodologia de desenvolvimento'),
(11, 'Planejamento adequado'),
(12, 'Relatório de progresso atualizado'),
(13, 'Monitoramento e controle efetivo do projeto'),
(14, 'Gerenciamento de riscos'),
(15, 'Controle efetivo de mudanças  e gerenciamento de configuração'),
(16, 'Equipe comprometida e motivada'),
(17, 'Bom gerenciamento da qualidade'),
(18, 'Designação clara de papéis e responsabilidades'),
(19, 'Ferramentas de apoio');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fator_perfil`
--

CREATE TABLE IF NOT EXISTS `fator_perfil` (
`id` int(11) NOT NULL,
  `perfil_id` int(11) NOT NULL,
  `fator_critico_id` int(11) NOT NULL,
  `prioridade` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Extraindo dados da tabela `fator_perfil`
--

INSERT INTO `fator_perfil` (`id`, `perfil_id`, `fator_critico_id`, `prioridade`) VALUES
(39, 3, 1, 2),
(40, 3, 8, 5),
(41, 3, 2, 1),
(42, 3, 3, 7),
(43, 3, 4, 19),
(44, 3, 5, 8),
(45, 3, 6, 3),
(46, 3, 7, 11),
(47, 3, 9, 12),
(48, 3, 10, 13),
(49, 3, 11, 6),
(50, 3, 12, 14),
(51, 3, 13, 9),
(52, 3, 14, 15),
(53, 3, 15, 16),
(54, 3, 16, 4),
(55, 3, 17, 10),
(56, 3, 18, 17),
(57, 3, 19, 18);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fator_pergunta`
--

CREATE TABLE IF NOT EXISTS `fator_pergunta` (
`id` int(11) NOT NULL,
  `pergunta_id` int(11) NOT NULL,
  `fator_critico_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=87 ;

--
-- Extraindo dados da tabela `fator_pergunta`
--

INSERT INTO `fator_pergunta` (`id`, `pergunta_id`, `fator_critico_id`) VALUES
(1, 1, 6),
(2, 2, 4),
(3, 2, 6),
(4, 2, 2),
(5, 3, 6),
(6, 3, 18),
(7, 3, 5),
(8, 4, 6),
(9, 4, 2),
(10, 4, 1),
(11, 5, 18),
(12, 5, 8),
(13, 5, 11),
(14, 6, 8),
(15, 6, 11),
(16, 7, 16),
(17, 8, 16),
(18, 9, 11),
(19, 9, 3),
(20, 10, 14),
(21, 10, 7),
(22, 10, 11),
(23, 11, 17),
(24, 11, 15),
(25, 11, 14),
(26, 11, 13),
(27, 11, 11),
(28, 12, 14),
(29, 12, 11),
(30, 12, 3),
(31, 12, 1),
(32, 13, 19),
(33, 13, 7),
(34, 13, 8),
(35, 14, 7),
(36, 14, 11),
(37, 15, 14),
(38, 15, 11),
(39, 16, 14),
(40, 16, 1),
(41, 17, 13),
(42, 17, 11),
(43, 17, 3),
(44, 18, 6),
(45, 18, 14),
(46, 18, 13),
(47, 18, 12),
(48, 19, 6),
(49, 19, 14),
(50, 19, 13),
(51, 19, 12),
(52, 20, 14),
(53, 20, 13),
(54, 20, 11),
(55, 21, 17),
(56, 21, 6),
(57, 21, 2),
(58, 21, 1),
(59, 22, 6),
(60, 22, 18),
(61, 23, 11),
(62, 23, 3),
(63, 23, 1),
(64, 24, 17),
(65, 24, 11),
(66, 25, 17),
(67, 25, 14),
(68, 25, 11),
(69, 25, 3),
(70, 26, 17),
(71, 26, 14),
(72, 26, 11),
(73, 26, 3),
(74, 27, 11),
(75, 27, 3),
(76, 27, 5),
(77, 28, 17),
(78, 28, 11),
(79, 28, 3),
(80, 30, 10),
(81, 30, 8),
(82, 30, 11),
(83, 31, 17),
(84, 31, 1),
(85, 29, 9),
(86, 29, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `fator_pratica`
--

CREATE TABLE IF NOT EXISTS `fator_pratica` (
`id` int(11) NOT NULL,
  `fator_critico_id` int(11) NOT NULL,
  `pratica_agil_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Extraindo dados da tabela `fator_pratica`
--

INSERT INTO `fator_pratica` (`id`, `fator_critico_id`, `pratica_agil_id`) VALUES
(4, 6, 3),
(5, 2, 3),
(6, 7, 3),
(7, 11, 3),
(8, 1, 3),
(9, 4, 4),
(10, 15, 4),
(11, 5, 4),
(12, 10, 5),
(13, 9, 5),
(14, 16, 5),
(15, 3, 6),
(16, 1, 5),
(17, 5, 5),
(18, 15, 5),
(19, 14, 6),
(20, 7, 6),
(21, 11, 6),
(25, 7, 8),
(26, 11, 8),
(27, 3, 8),
(28, 18, 9),
(29, 8, 9),
(30, 3, 9),
(31, 4, 10),
(32, 6, 10),
(33, 19, 10),
(34, 14, 10),
(35, 13, 10),
(36, 12, 10),
(37, 10, 11),
(38, 17, 14),
(39, 9, 15),
(40, 15, 16),
(41, 16, 20),
(42, 17, 21),
(43, 15, 19),
(44, 17, 18),
(45, 15, 18),
(46, 19, 18),
(47, 15, 17),
(48, 10, 12),
(49, 6, 12),
(50, 14, 12),
(51, 13, 12),
(52, 12, 12),
(53, 6, 13),
(54, 6, 11),
(55, 6, 4),
(56, 1, 4),
(57, 6, 14),
(58, 17, 15),
(59, 14, 11),
(60, 13, 11),
(61, 12, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
`id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'administrador', 'Administradores do sistema'),
(2, 'gerente', 'usuários do sistema');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
`id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `metodologia_agil`
--

CREATE TABLE IF NOT EXISTS `metodologia_agil` (
`id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` text
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `metodologia_agil`
--

INSERT INTO `metodologia_agil` (`id`, `nome`, `descricao`) VALUES
(1, 'Scrum', '<div>	Scrum &eacute; uma metodologia &aacute;gil para gest&atilde;o e planejamento de projetos e desenvolvimento &aacute;gil de software. &Eacute; utilizado para trabalhos complexos nos quais &eacute; imposs&iacute;vel predizer tudo o que ir&aacute; ocorrer. O Scrum consiste nos times do Scrum associados a pap&eacute;is, eventos, artefatos e regras. As regras do Scrum integram os eventos, pap&eacute;is e artefatos, administrando as rela&ccedil;&otilde;es e intera&ccedil;&otilde;es entre eles.</div><div>	&nbsp;</div><div>	No Scrum, os projetos s&atilde;o divididos em ciclos (tipicamente mensais) chamados de Sprints. Cada Sprint representa uma itera&ccedil;&atilde;o dentro da qual um conjunto de atividades deve ser executado.&nbsp;</div><div>	&nbsp;</div><div>	O Time Scrum &eacute; composto pelo Product Owner, o Time de Desenvolvimento e o Scrum Master. O Product Owner, ou dono do produto, &eacute; o respons&aacute;vel por maximizar o valor do produto e do trabalho do Time de Desenvolvimento. O Time de Desenvolvimento consiste de profissionais que realizam o trabalho de entregar uma vers&atilde;o us&aacute;vel que potencialmente &nbsp;incrementa o produto &quot;pronto&quot; ao final de cada Sprint. O Scrum Master &eacute; respons&aacute;vel por garantir que o Scrum seja entendido e aplicado, e ajuda aqueles que est&atilde;o fora do Time Scrum a entender quais as suas intera&ccedil;&otilde;es com o Time Scrum s&atilde;o &uacute;teis e quais n&atilde;o s&atilde;o. O Scrum Master ajuda todos a mudarem estas intera&ccedil;&otilde;es para maximizar o valor criado pelo Time Scrum.</div>'),
(2, 'XP', '<div>	Extreme Programming (XP) &eacute; uma metodologia de desenvolvimento de software que adota os valores de comunica&ccedil;&atilde;o, simplicidades, feedback e coragem. Estes quatro valores servem como crit&eacute;rios que norteiam as pessoas envolvidas no desenvolvimento de software.</div><div>	&nbsp;</div><div>	O objetivo principal do XP &eacute; levar ao extremo um conjunto de pr&aacute;ticas que s&atilde;o ditas como boas na engenharia de software. &nbsp;O XP preconiza ciclos curtos que fornecem previsibilidade, redu&ccedil;&atilde;o de incertezas ou riscos, simplicidade e melhorias constantes de c&oacute;digo (refatora&ccedil;&atilde;o) para facilitar mudan&ccedil;as, &nbsp;testes automatizados e integra&ccedil;&atilde;o cont&iacute;nua para aumentar a confian&ccedil;a.</div>'),
(3, 'Kanban', '<div>	O Kanban &eacute; uma metodologia de sinaliza&ccedil;&atilde;o para controle de fluxo de opera&ccedil;&atilde;o aplicada em ind&uacute;strias de forma geral. No setor de Tecnologia da Informa&ccedil;&atilde;o, kanban tamb&eacute;m &eacute; uma ferramenta visual de gest&atilde;o de fluxo de desenvolvimento. &Eacute; uma das metodologias de desenvolvimento de software menos prescritivas, se tornando adapt&aacute;vel a quase qualquer tipo de cultura.</div><div>	&nbsp;</div><div>	O Kanban &eacute; baseado na ideia onde atividades em andamento devem ser limitadas. Um novo item s&oacute; pode ser iniciado quando o item em andamento &eacute; finalizado ou quando uma fun&ccedil;&atilde;o autom&aacute;tica inicia o mesmo instantaneamente. Basicamente, o Kanban tem como principal objetivo transformar o trabalho em andamento vis&iacute;vel para toda a equipe, criando um sinal visual que indica se o novo trabalho pode ou n&atilde;o ser iniciado e se o limite acordado para cada fase est&aacute; sendo respeitado. Uma outra caracter&iacute;stica importante do modelo Kanban &eacute; o conceito de &quot;puxar tarefa&quot; quando h&aacute; capacidade de process&aacute;-la. Esse recurso vai de encontro ao tradicional modelo de &quot;empurrar tarefa&quot; conforme sua demanda, mantendo assim o bom desempenho da equipe.</div><div>	&nbsp;</div><div>	Um dos principais mecanismos utilizados pelo Kanban &eacute; o Quadro Kanban, que s&atilde;o quadros de cart&otilde;es e post-its dispostos de acordo com o andamento do projeto para o controle visual do desenvolvimento de software &aacute;gil e do trabalho em andamento.</div>'),
(4, 'Lean', '<div>	A metodologia Lean &eacute; uma estrat&eacute;gia de neg&oacute;cios que busca aumentar a satisfa&ccedil;&atilde;o do cliente atrav&eacute;s de um melhor aproveitamento de recursos. No contexto de software, &nbsp;A metodologia Lean &eacute; a aplica&ccedil;&atilde;o dos conceitos do sistema de produ&ccedil;&atilde;o da Toyota para o desenvolvimento de software, e quando aplicada corretamente tem como consequ&ecirc;ncia um desenvolvimento de alta qualidade que &eacute; feito rapidamente e com um baixo custo.</div><div>	&nbsp;</div><div>	&Eacute; distribu&iacute;da em sete princ&iacute;pios: eliminar o desperd&iacute;cio, amplificar o aprendizado, adiar comprometimentos e manter a flexibilidade, entregar r&aacute;pido, tornar a equipe respons&aacute;vel, construir integridade e visualizar o todo.</div>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
`id` int(11) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  `usuario_id` int(11) unsigned NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `tamanho_equipe` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `ativo`, `usuario_id`, `nome`, `tamanho_equipe`) VALUES
(3, 1, 2, 'Perfil UNIRIO', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pergunta`
--

CREATE TABLE IF NOT EXISTS `pergunta` (
`id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` text,
  `grupo` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Extraindo dados da tabela `pergunta`
--

INSERT INTO `pergunta` (`id`, `nome`, `descricao`, `grupo`) VALUES
(1, 'Falta de comunicação entre a equipe', 'Nos meus projetos foi verificado que a equipe não se comunica o suficiente. Os membros estão frequentemente desinformados sobre as atividades dos seus companheiros de equipe, podendo resultar em problemas como perda de tempo de projeto e retrabalho.', 1),
(2, 'Falta de apoio da alta administração', 'Nos meus projetos foi verificado que a alta administração não reconheceu a importância dos produtos/serviços que seriam gerados, dessa forma não apoiou a execução dos projetos de forma adequada.\r\n', 1),
(3, 'Falta de envolvimento do cliente', 'Nos meus projetos foi verificado que o cliente não se envolve o suficiente, não cumprindo tarefas de sua responsabilidade (por exemplo, reuniões, feedback, homologação).\r\n', 1),
(4, 'Falta de comunicação com o cliente', 'Nos meus projetos foi verificado que a comunicação com o cliente não foi eficaz, podendo resultar em uma expectativa muito alta ou muito baixa do cliente em relação ao projeto.', 1),
(5, 'Funcionários inadequados para o projeto', 'Nos meus projetos foi verificado que a equipe alocada não possuía o conhecimento e/ou experiência necessários.', 1),
(6, 'Funcionários insuficientes', 'Nos meus projetos foi verificado que a equipe alocada para o projeto foi insuficiente.\r\n', 1),
(7, 'Falta de comprometimento da equipe', 'Nos meus projetos foi verificado que houve falta de comprometimento por parte da equipe.\r\n', 1),
(8, 'Falta de motivação da equipe', 'Nos meus projetos foi verificado que a equipe estava desmotivada.', 1),
(9, 'Estimativas de tempo e prazos não condizem com o tempo real das entregas', 'Nos meus projetos foi verificado que o tempo estimado para a conclusão das atividades foi muito maior/menor do que o tempo efetivamente gasto na atividade até a entrega.\r\n', 3),
(10, 'Grande impacto no orçamento devido à mudanças nas necessidades do cliente durante o desenvolvimento', 'Nos meus projetos foi verificado que mudanças nas necessidades do cliente alteraram de forma drástica o orçamento.\r\n', 3),
(11, 'Grande impacto no prazo de entrega do projeto devido à mudanças nas necessidades do cliente durante o desenvolvimento', 'Nos meus projetos foi verificado que mudanças decorrentes de novas necessidades do cliente alteraram bastante o prazo de entrega.\r\n', 3),
(12, 'Requisitos indefinidos no início do projeto', 'Nos meus projetos a maioria dos requisitos ainda não são bem definidos no início do desenvolvimento, podendo dificultar a medição do esforço e estimativas de prazos. ', 3),
(13, 'Recursos limitados para o projeto', 'Nos meus projetos foi verificado que recursos necessários para o projeto foram comprometidos ou limitados.\r\n', 3),
(14, 'Orçamento inadequado', 'Nos meus projetos foi verificado que o orçamento estipulado foi inadequado para a realidade do projeto.\r\n', 3),
(15, 'Orçamento estourou o limite estipulado', 'Nos meus projetos foi verificado que o orçamento estourou o limite estipulado.', 3),
(16, 'Alto índice de mudanças nas necessidades do cliente durante o desenvolvimento', 'Nos meus projetos foi verificado que houve um alto índice de mudanças nas necessidades do cliente durante o desenvolvimento, podendo ter dificultado a conclusão e a entrega.', 3),
(17, 'Planejamento inadequado', 'Nos meus projetos foi verificado que o planejamento não foi realizado de forma adequada.', 3),
(18, 'Relatórios defasados', 'Nos meus projetos foi verificado que os relatórios de progresso do projeto submetidos estavam defasados ou com dados incorretos.', 3),
(19, 'Monitoramento e controle do projeto insuficientes', 'Nos meus projetos foi verificado que o monitoramento e controle foram insuficientes ou não foram executados da forma correta. Pode não ter havido sinalização e acompanhamento dos projetos e as partes envolvidas, com isso, não tinham conhecimento do estado geral do projeto. \r\n', 3),
(20, 'Ausência ou má execução do gerenciamento de riscos', 'Nos meus projetos foi verificado que não houve uma gerência adequada de riscos. Os riscos não foram gerenciados de modo a mitigar os impactos no projeto ou os riscos errados foram tratados.\r\n', 3),
(21, 'Controle de qualidade ausente ou insuficiente', 'Nos meus projetos foi verificado que a qualidade do produto/ serviço gerado não correspondia às expectativas do cliente.', 3),
(22, 'Papéis e responsabilidades não foram claramente definidos', 'Nos meus projetos foi verificado que os papéis ou responsabilidades não foram claramente definidos para todos os membros da equipe.\r\n', 3),
(23, 'Estimativas/prazos foram exigidos antes da definição completa dos requisitos', 'Nos meus projetos foi verificado que as estimativas foram feitas e exigidas antes que os requisitos estivessem completamente definidos, resultando em prazos imprecisos.\r\n', 3),
(24, 'Testes do software foram insuficientes', 'Nos meus projetos foi verificado que os testes foram deixados para o final do desenvolvimento, resultando em um sistema com muitos bugs e problemas.\r\n', 3),
(25, 'Testes do software impactaram prazos de entrega', 'Nos meus projetos foi verificado que os testes foram deixados para o final do desenvolvimento, atrasando prazos de entrega.\r\n', 3),
(26, 'Testes do software impactaram prazos de entrega', 'Nos meus projetos foi verificado que os testes foram deixados para o final do desenvolvimento, atrasando prazos de entrega.\r\n', 3),
(27, 'Prazos agressivos resultaram em prazos não cumpridos', 'Nos meus projetos, por necessidade do cliente foi dado um prazo agressivo que não correspondia com as estimativas reais das atividades, resultando em prazos não cumpridos.\r\n', 3),
(28, 'Prazos agressivos resultaram em problemas no software', 'Nos meus projetos, por necessidade do cliente foi dado um prazo agressivo que não correspondia com as estimativas reais das atividades, resultando em problemas no software.\r\n', 3),
(29, 'Falta de familiaridade com as tecnologias utilizadas', 'Nos meus projetos foi verificado que a equipe não estava familiarizada com as tecnologias utilizadas ou ocorreram mudanças nas tecnologias utilizadas ao longo da execução.\r\n', 2),
(30, 'Falta de familiaridade com a metodologia utilizada', 'Nos meus projetos foi verificado que a equipe não estava familiarizada com as metodologias utilizadas no projeto, ou ocorreram mudanças na metodologia utilizada.', 2),
(31, 'Testes realizados não foram efetivos', 'Nos meus projetos foi verificado que os testes realizados não foram efetivos, comprometendo a qualidade do software.\r\n', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pergunta_grupo`
--

CREATE TABLE IF NOT EXISTS `pergunta_grupo` (
`id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `descricao` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `pergunta_grupo`
--

INSERT INTO `pergunta_grupo` (`id`, `nome`, `descricao`) VALUES
(1, 'Pessoas', 'relacionado à gestão de pessoas, como comunicação e quadro dos profissionais envolvidos nos projetos.'),
(2, 'Técnicos', 'relacionado aos aspectos técnicos do projeto, como tecnologias utilizadas e qualidade.'),
(3, 'Processos', 'relacionado aos processos de projetos, como planejamento, metodologias e gerenciamento de riscos.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pratica_agil`
--

CREATE TABLE IF NOT EXISTS `pratica_agil` (
`id` int(11) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` text,
  `peso` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `pratica_agil`
--

INSERT INTO `pratica_agil` (`id`, `nome`, `descricao`, `peso`) VALUES
(3, 'Backlog do Produto', '<div>\r\n	O Backlog &eacute; uma lista de todas as funcionalidades pensadas para um produto. Inicialmente, o Backlog come&ccedil;a com as necessidades mais b&aacute;sicas, mas geralmente muda com o tempo, de acordo com o aprendizado do time sobre o produto que est&aacute; desenvolvendo e seus usu&aacute;rios (reais ou potenciais).</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Manter uma lista priorizada &eacute; importante para o fluxo de produ&ccedil;&atilde;o, para dar mais visibilidade &agrave;s trocas que acontecem e aos efeitos dessas mudan&ccedil;as. Fazem parte do Backlog tarefas t&eacute;cnicas ou atividades diretamente relacionadas &agrave;s funcionalidades solicitadas.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 5),
(4, 'Cliente Presente', '<div>\r\n	&Eacute; fundamental a participa&ccedil;&atilde;o do cliente durante todo o desenvolvimento do projeto. O cliente deve estar sempre dispon&iacute;vel para sanar todas as d&uacute;vidas de requisitos, evitando atrasos e at&eacute; mesmo constru&ccedil;&otilde;es erradas. Uma ideia interessante &eacute; manter o cliente como parte integrante da equipe que far&aacute; os testes do software.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 1),
(5, 'Pequenas Liberações', '<div>\r\n	A pr&aacute;tica de Pequenas Libera&ccedil;&otilde;es, tamb&eacute;m conhecida como Entregas Curtas, incentiva o time de desenvolvimento a entregar novas vers&otilde;es de software de maneira cont&iacute;nua, reduzindo o tempo do ciclo de vida da implanta&ccedil;&atilde;o de funcionalidades e trazendo mais agilidade para os neg&oacute;cios. Ciclos curtos podem reduzir riscos, ajudar a lidar com mudan&ccedil;as nos requisitos e reduzir o impacto de erros de planejamento. Ao final de cada release, o cliente rev&ecirc; todo o produto podendo identificar defeitos e fazer ajustes nos requisitos futuros.</div>\r\n', 4),
(6, 'Reuniões de Planejamento', '<div>\r\n	Nas reuni&otilde;es de planejamento, s&atilde;o escolhidos os itens da lista (Backlog) com prioridade mais alta para o desenvolvimento. Estes itens s&atilde;o quebrados em pequenas tarefas, que ser&atilde;o executadas durante a pr&oacute;xima itera&ccedil;&atilde;o. Nesta reuni&atilde;o, o cliente tem a fun&ccedil;&atilde;o de ajudar a definir e priorizar os itens do Backlog.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 1),
(8, 'Jogos de Planejamento', '<div>\r\n	Jogos de planejamento auxiliam a manter o foco no que &eacute; de maior valor para o cliente, e s&atilde;o executados sempre no in&iacute;cio de uma itera&ccedil;&atilde;o ou release. &Eacute; feita uma determina&ccedil;&atilde;o r&aacute;pida do escopo do release, atrav&eacute;s da combina&ccedil;&atilde;o de estimativas e prioridades do neg&oacute;cio.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Nos jogos de planejamento,os desenvolvedores estimam o tempo para o desenvolvimento das funcionalidades atrav&eacute;s de pontos. &nbsp;O cliente &eacute; respons&aacute;vel por definir quais s&atilde;o as funcionalidades a serem entregues no pr&oacute;ximo release, priorizando as que possuem maior valor. As estimativas podem ser refeitas durante as itera&ccedil;&otilde;es &agrave; medida que os programadores aprenderem mais sobre o sistema.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Como exemplos de jogos de planejamento podem ser citados o Planning Poker e a T&eacute;cnica Pomodoro.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 1),
(9, 'Equipe Completa', '<div>\r\n	Refere-se &agrave; pr&aacute;tica de incluir todos os perfis e perspectivas necess&aacute;rios na equipe para que ela possa ter bom desempenho, enfatizando o esp&iacute;rito de equipe, com todos os seus membros compartilhando um prop&oacute;sito e apoiando-se mutuamente.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Clientes, usu&aacute;rios e demais interessados devem ter um envolvimento direto no projeto, a fim de possibilitar entender o comportamento do sistema mais cedo no ciclo de vida.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 4),
(10, 'Visibilidade do Projeto', '<div>\r\n	Projetos &aacute;geis por sua natureza est&atilde;o continuamente mudando (planos, modelos, c&oacute;digo e demais artefatos). O objetivo da pr&aacute;tica de Visibilidade do Projeto &eacute; manter e monitorar a qualquer tempo as medi&ccedil;&otilde;es do progresso do projeto. As informa&ccedil;&otilde;es devem estar acess&iacute;veis para todos os envolvidos no projeto. &nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Pode ser criado um painel na web para manter a qualquer tempo o status e as m&eacute;tricas relacionadas com o progresso do projeto. M&uacute;ltiplos pain&eacute;is podem ser usados para disponibilizar diferentes tipos de informa&ccedil;&atilde;o que atendam a todos os n&iacute;veis organizacionais necess&aacute;rios.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Para esta pr&aacute;tica s&atilde;o utilizadas ferramentas de apoio como quadro de tarefas do Scrum ou Kanban, Gr&aacute;fico Burndown e Hist&oacute;rias de Usu&aacute;rios.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 4),
(11, 'Reuniões Diárias', '<div>\r\n	<p>\r\n		Reuni&otilde;es Di&aacute;rias duram poucos minutos (geralmente de 15 a 20 minutos), incentivam a comunica&ccedil;&atilde;o do time e a entender como caminha a evolu&ccedil;&atilde;o do projeto. Nestas reuni&otilde;es, cada um dos participantes responde algumas quest&otilde;es fundamentais sobre sua atividade, como o que foi feito no dia anterior, o que ser&aacute; feito hoje e impedimentos para a execu&ccedil;&atilde;o da atividade.</p>\r\n	Reuni&otilde;es Di&aacute;rias podem auxiliar o gerenciamento e tratamento dos riscos do projeto, pois a equipe do projeto fica sabendo rapidamente de qualquer problema, que ter&aacute; sido detectado h&aacute; no m&aacute;ximo um dia de trabalho.&nbsp;</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 4),
(12, 'Scrum de Scrums', '<div>\r\n	O objetivo desta pr&aacute;tica &eacute; dar suporte em situa&ccedil;&otilde;es na qual a equipe &eacute; muito grande e necessita ser quebrada em v&aacute;rias equipes que precisam interagir constantemente em prol do progresso do projeto. O objetivo do Scrum de Scrums &eacute; similar ao das Reuni&otilde;es Di&aacute;rias, mas numa escalar maior. Enquanto as Reuni&otilde;es Di&aacute;rias s&atilde;o mais vi&aacute;veis com equipes pequenas de modo a ser uma reuni&atilde;o curta, o Scrum de Scrums visa realizar uma reuni&atilde;o mais especializada com o objetivo de manter as equipes atualizadas em rela&ccedil;&atilde;o aos acontecimentos desde a &uacute;ltima reuni&atilde;o.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	No Scrum de Scrums, basicamente, divide-se um time Scrum em dois ou mais times Scrum. Cada um dos times possui suas pr&oacute;prias atividades, reuni&otilde;es di&aacute;rias, itera&ccedil;&otilde;es (Sprints), e seus pr&oacute;prios desenvolvedores e l&iacute;der de equipe. O l&iacute;der de equipe de cada time comparece &agrave; reuni&atilde;o Scrum de Scrums, geralmente efetuada de uma &agrave; tr&ecirc;s vezes por semana, para comunicar a todos os times sobre as realiza&ccedil;&otilde;es passadas e futuras de todo o projeto.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 1),
(13, 'Propriedade Coletiva do Código', '<div>\r\n	Com a utiliza&ccedil;&atilde;o da pr&aacute;tica de Propriedade Coletiva do C&oacute;digo, todos t&ecirc;m acesso e autoriza&ccedil;&atilde;o para editar qualquer parte do c&oacute;digo da aplica&ccedil;&atilde;o, a qualquer momento. Ou seja, a propriedade do c&oacute;digo &eacute; coletiva e todos s&atilde;o igualmente respons&aacute;veis por todas as partes. Esta pr&aacute;tica pode contribuir para o ganho de tempo e dissemina&ccedil;&atilde;o do conhecimento, al&eacute;m da frequente identifica&ccedil;&atilde;o de oportunidades de melhoria e refatora&ccedil;&atilde;o do c&oacute;digo.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 4),
(14, 'Desenvolvimento Orientado por Comportamento', '<div>\r\n	O desenvolvimento orientado por comportamento (BDD) &eacute; usado para integrar regras de neg&oacute;cios com a linguagem de programa&ccedil;&atilde;o, atrav&eacute;s da cria&ccedil;&atilde;o de testes que focam o comportamento do software. Al&eacute;m disso, pode melhorar a comunica&ccedil;&atilde;o entre as equipes de desenvolvimento e testes, aumentando o compartilhamento de conhecimento.&nbsp;&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 1),
(15, 'Programação em Pares', '<div>\r\n	A Programa&ccedil;&atilde;o em Pares sugere que todo e qualquer c&oacute;digo produzido no projeto seja sempre implementado por duas pessoas juntas, diante do mesmo computador, revezando-se no teclado. Esta pr&aacute;tica pode ajudar os desenvolvedores na cria&ccedil;&atilde;o de solu&ccedil;&otilde;es mais simples, mais adapt&aacute;veis e mais f&aacute;ceis de manter. Um dos principais benef&iacute;cios da programa&ccedil;&atilde;o em pares &eacute; a permanente inspe&ccedil;&atilde;o de c&oacute;digo que ocorre durante seu uso, podendo reduzir a incid&ecirc;ncia de bugs em um sistema.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 1),
(16, 'Refatoração', '<div>\r\n	A pr&aacute;tica de Refatora&ccedil;&atilde;o consiste em alterar pequenas partes do sistema, frequentemente, sempre que encontram uma oportunidade para melhorar o c&oacute;digo, tornando-o mais limpo, mais claro e mais f&aacute;cil de ser compreendido. Tais altera&ccedil;&otilde;es n&atilde;o mudam o comportamento das funcionalidades, apenas melhoram a estrutura do c&oacute;digo. Agindo assim de forma sistem&aacute;tica e com frequ&ecirc;ncia, as equipes de desenvolvimento investem para que o software se mantenha sempre f&aacute;cil de alterar.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 3),
(17, 'Padronização de Código', '<div>\r\n	Pelo fato de os desenvolvedores programarem diferentes partes do sistema com v&aacute;rios membros da equipe, a ado&ccedil;&atilde;o de padr&otilde;es de c&oacute;digo &eacute; bastante interessante. Eles facilitam o entendimento do c&oacute;digo, aumentam a legibilidade e melhoram a consist&ecirc;ncia entre membros da equipe. Os padr&otilde;es devem ser f&aacute;ceis de serem seguidos e devem ser adotados voluntariamente. Deve ser acordado pela equipe, assegurando que a comunica&ccedil;&atilde;o possa ser feita via c&oacute;digo, al&eacute;m de levar os desenvolvedores a entender facilmente o c&oacute;digo de seus colegas.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 2),
(18, 'Integração Contínua', '<div>\r\n	<p>\r\n		Integra&ccedil;&atilde;o Cont&iacute;nua consiste em integrar o trabalho diversas vezes ao dia, contribuindo para que a base de c&oacute;digo permane&ccedil;a consistente ao final de cada integra&ccedil;&atilde;o e possibilitando que qualquer desenvolvedor possa obter todo o c&oacute;digo do projeto, a qualquer momento.</p>\r\n	Para esta pr&aacute;tica s&atilde;o utilizadas ferramentas de apoio como sistemas de controle de vers&otilde;es ou reposit&oacute;rio de c&oacute;digo.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 5),
(19, 'Design Simples', '<div>\r\n	A pr&aacute;tica foca em manter o design simples, podendo contribuir para um desenvolvimento adapt&aacute;vel &agrave;s necessidades. O design &eacute; mantido apropriado para que o projeto requer no momento. Deve ser revisado iterativamente e incrementalmente para garantir que continua apropriado.</div>\r\n', 3),
(20, 'Ritmo Sustentável', '<div>\r\n	A pr&aacute;tica foca em trabalhar com qualidade, buscando ter ritmo de trabalho saud&aacute;vel (40 horas/semana, 8 horas/dia).</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Busca-se promover um ritmo constante e sustent&aacute;vel para o trabalho do time que desenvolve o produto, o que se torna poss&iacute;vel quando esse ritmo &eacute; apoiado por toda a cadeia, incluindo usu&aacute;rios e patrocinadores. No entanto, ao se exigir do time um compromisso com mais trabalho do que ele &eacute; capaz de produzir, s&atilde;o muitas vezes adotadas as horas extras, o trabalho em fins de semana e a pressa exagerada para se cumprir o prazo de entrega, por exemplo. Essas pr&aacute;ticas podem levar &agrave; insatisfa&ccedil;&atilde;o dos membros do time de desenvolvimento, a uma menor produtividade e a uma menor qualidade no produto gerado.</div>\r\n<div>\r\n	&nbsp;</div>\r\n', 4),
(21, 'Desenvolvimento Orientado por Testes', '<p>\r\n	O Desenvolvimento Orientado por Testes (TDD) &eacute; uma t&eacute;cnica de desenvolvimento de software que consiste em pequenas itera&ccedil;&otilde;es de desenvolvimento onde o caso de teste &eacute; escrito antes de ser implementada a pr&oacute;pria funcionalidade.</p>\r\n<p>\r\n	O processo de desenvolvimento de software resume-se &agrave; execu&ccedil;&atilde;o repetida dos passos: escrever um teste unit&aacute;rio que falhe antes de escrever qualquer c&oacute;digo funcional; escrever o c&oacute;digo funcional at&eacute; que o teste unit&aacute;rio passe; no final, caso seja necess&aacute;rio, refatorar o c&oacute;digo, assegurando que os testes unit&aacute;rios continuam todos a ter sucesso.</p>\r\n<p>\r\n	A utiliza&ccedil;&atilde;o do TDD pode diminuir o n&uacute;mero de bugs e permitir que a aplica&ccedil;&atilde;o final seja mais robusta, uma vez que se garante que o c&oacute;digo para o qual se implementou testes funciona.</p>\r\n', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pratica_metodologia`
--

CREATE TABLE IF NOT EXISTS `pratica_metodologia` (
`id` int(11) NOT NULL,
  `pratica_agil_id` int(11) NOT NULL,
  `metodologia_agil_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Extraindo dados da tabela `pratica_metodologia`
--

INSERT INTO `pratica_metodologia` (`id`, `pratica_agil_id`, `metodologia_agil_id`) VALUES
(1, 3, 1),
(2, 4, 1),
(3, 9, 1),
(4, 5, 1),
(5, 6, 1),
(6, 11, 1),
(7, 12, 1),
(8, 10, 1),
(9, 4, 2),
(10, 21, 2),
(11, 19, 2),
(12, 9, 2),
(13, 18, 2),
(14, 8, 2),
(15, 17, 2),
(16, 5, 2),
(17, 15, 2),
(18, 13, 2),
(19, 16, 2),
(20, 11, 2),
(21, 20, 2),
(22, 10, 2),
(23, 3, 3),
(24, 10, 3),
(25, 4, 4),
(26, 21, 4),
(27, 18, 4),
(28, 10, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta`
--

CREATE TABLE IF NOT EXISTS `resposta` (
`id` int(11) NOT NULL,
  `pergunta_id` int(11) NOT NULL,
  `perfil_id` int(11) DEFAULT NULL,
  `resposta` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Extraindo dados da tabela `resposta`
--

INSERT INTO `resposta` (`id`, `pergunta_id`, `perfil_id`, `resposta`) VALUES
(26, 1, 3, 4),
(27, 7, 3, 2),
(28, 8, 3, 2),
(29, 9, 3, 5),
(30, 12, 3, 3),
(31, 17, 3, 5),
(33, 19, 3, 4),
(34, 24, 3, 4),
(35, 28, 3, 5),
(36, 31, 3, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `photo`) VALUES
(1, '127.0.0.1', 'administrador', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'administrador@uniriotec.br', '', '', NULL, '', 1268889823, 1467813968, 1, 'Administrador', 'Sistema', '', '0', ''),
(2, '127.0.0.1', 'gerente', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'gerente@uniriotec.br', '', NULL, NULL, '', 1268889823, 1467814048, 1, 'Gerente', 'Sistema', '', '0', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fator_critico`
--
ALTER TABLE `fator_critico`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fator_perfil`
--
ALTER TABLE `fator_perfil`
 ADD PRIMARY KEY (`id`), ADD KEY `perfil_id` (`perfil_id`), ADD KEY `fator_critico_id` (`fator_critico_id`);

--
-- Indexes for table `fator_pergunta`
--
ALTER TABLE `fator_pergunta`
 ADD PRIMARY KEY (`id`), ADD KEY `pergunta_id` (`pergunta_id`), ADD KEY `fator_critico_id` (`fator_critico_id`);

--
-- Indexes for table `fator_pratica`
--
ALTER TABLE `fator_pratica`
 ADD PRIMARY KEY (`id`), ADD KEY `fator_critico_id` (`fator_critico_id`), ADD KEY `pratica_agil_id` (`pratica_agil_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metodologia_agil`
--
ALTER TABLE `metodologia_agil`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_perfil_0` (`usuario_id`);

--
-- Indexes for table `pergunta`
--
ALTER TABLE `pergunta`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pergunta_grupo`
--
ALTER TABLE `pergunta_grupo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pratica_agil`
--
ALTER TABLE `pratica_agil`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pratica_metodologia`
--
ALTER TABLE `pratica_metodologia`
 ADD PRIMARY KEY (`id`), ADD KEY `pratica_agil_id` (`pratica_agil_id`), ADD KEY `metodologia_agil_id` (`metodologia_agil_id`);

--
-- Indexes for table `resposta`
--
ALTER TABLE `resposta`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_resposta_0` (`pergunta_id`), ADD KEY `FK_resposta_1` (`perfil_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fator_critico`
--
ALTER TABLE `fator_critico`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `fator_perfil`
--
ALTER TABLE `fator_perfil`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `fator_pergunta`
--
ALTER TABLE `fator_pergunta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `fator_pratica`
--
ALTER TABLE `fator_pratica`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `metodologia_agil`
--
ALTER TABLE `metodologia_agil`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pergunta`
--
ALTER TABLE `pergunta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `pergunta_grupo`
--
ALTER TABLE `pergunta_grupo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pratica_agil`
--
ALTER TABLE `pratica_agil`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `pratica_metodologia`
--
ALTER TABLE `pratica_metodologia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `resposta`
--
ALTER TABLE `resposta`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `fator_perfil`
--
ALTER TABLE `fator_perfil`
ADD CONSTRAINT `fator_perfil_ibfk_1` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `fator_perfil_ibfk_2` FOREIGN KEY (`fator_critico_id`) REFERENCES `fator_critico` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `fator_pergunta`
--
ALTER TABLE `fator_pergunta`
ADD CONSTRAINT `fator_pergunta_ibfk_2` FOREIGN KEY (`fator_critico_id`) REFERENCES `fator_critico` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `fator_pergunta_ibfk_3` FOREIGN KEY (`pergunta_id`) REFERENCES `pergunta` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `fator_pratica`
--
ALTER TABLE `fator_pratica`
ADD CONSTRAINT `fator_pratica_ibfk_1` FOREIGN KEY (`fator_critico_id`) REFERENCES `fator_critico` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `fator_pratica_ibfk_2` FOREIGN KEY (`pratica_agil_id`) REFERENCES `pratica_agil` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `perfil`
--
ALTER TABLE `perfil`
ADD CONSTRAINT `perfil_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `pratica_metodologia`
--
ALTER TABLE `pratica_metodologia`
ADD CONSTRAINT `pratica_metodologia_ibfk_1` FOREIGN KEY (`pratica_agil_id`) REFERENCES `pratica_agil` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `pratica_metodologia_ibfk_2` FOREIGN KEY (`metodologia_agil_id`) REFERENCES `metodologia_agil` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `resposta`
--
ALTER TABLE `resposta`
ADD CONSTRAINT `resposta_ibfk_1` FOREIGN KEY (`pergunta_id`) REFERENCES `pergunta` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `resposta_ibfk_2` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
